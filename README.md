# Advent of Code 2021

## build / run / test

Prerequisites:

    $ cabal install hspec-discover

Using `cabal`:

    $ cabal build

    $ cabal test

    $ cabal run aoc2020 <day> <challenge>

    e.g.

    $ cabal run aoc2021 2 1

Run all programs:

    $ (source test.sh; run_all)
