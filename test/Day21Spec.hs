module Day21Spec (
  spec,
) where

import Control.Monad
import Data.Char (intToDigit, isLower)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Vector as Vec
import Day21
import Numeric (showIntAtBase)
import Test.Hspec
import Test.QuickCheck hiding (Small)
import Text.InterpolatedString.QM (qnb)

spec :: Spec
spec =
  describe "day21" $ do
    describe "part1" $ do
      it "example" $
        runGame [1..] (4,8) `shouldBe` (745, 993)

    describe "part2" $ do
      it "example" $
        runGame2 (4,8) `shouldBe` (444356092776315, 341960390180808)
