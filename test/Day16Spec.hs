module Day16Spec (
  spec,
) where

import Control.Monad
import Data.Char (intToDigit, isLower)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Vector as Vec
import Day16
import Numeric (showIntAtBase)
import Test.Hspec
import Test.QuickCheck hiding (Small)
import Text.InterpolatedString.QM (qnb)

spec :: Spec
spec =
  describe "day16" $ do
    describe "part 1" $ do
      describe "examples" $ do
        it "D2FE28" $
          parseInput "D2FE28"
            `shouldBe` Right (Packet 6 (PLiteral 2021))

        it "8A004A801A8002F478" $
          parseInput "8A004A801A8002F478"
            `shouldBe` Right
              ( Packet 4
                ( POperator 2
                  [ Packet 1
                    ( POperator 2
                      [ Packet 5
                        ( POperator 2
                          [Packet 6 (PLiteral 15)]
                        )
                      ]
                    )
                  ]
                )
              )

        it "620080001611562C8802118E34" $
          parseInput "620080001611562C8802118E34"
            `shouldBe` Right
              ( Packet 3
                ( POperator 0
                  [ Packet 0
                    ( POperator 0
                      [ Packet 0 ( PLiteral 10)
                      , Packet 5 ( PLiteral 11)
                      ]
                    )
                  , Packet 1
                    ( POperator 0
                      [ Packet 0 ( PLiteral 12)
                      , Packet 3 ( PLiteral 13)
                      ]
                    )
                  ]
                )
              )

        it "C0015000016115A2E0802F182340" $
          parseInput "C0015000016115A2E0802F182340"
            `shouldBe` Right
              ( Packet 6
                ( POperator 0
                  [ Packet 0
                    ( POperator 0
                      [ Packet 0 ( PLiteral 10)
                      , Packet 6 ( PLiteral 11 )
                      ]
                    )
                  , Packet 4
                    ( POperator 0
                      [ Packet 7 ( PLiteral 12)
                      , Packet 0 ( PLiteral 13)
                      ]
                    )
                  ]
                )
              )


        it "A0016C880162017C3686B18A3D4780" $
          parseInput "A0016C880162017C3686B18A3D4780"
            `shouldBe` Right
              ( Packet 5
                ( POperator 0
                  [ Packet 1
                    ( POperator 0
                      [ Packet 3
                        ( POperator 0
                          [ Packet 7 (PLiteral 6)
                          , Packet 6 (PLiteral 6)
                          , Packet 5 (PLiteral 12)
                          , Packet 2 (PLiteral 15)
                          , Packet 2 (PLiteral 15)
                          ]
                        )
                      ]
                    )
                  ]
                )
              )

    describe "part 2" $ do
      describe "examples" $ do
        it "C200B40A82" $ (eval <$> parseInput "C200B40A82") `shouldBe` Right 3
        it "04005AC33890" $ (eval <$> parseInput "04005AC33890") `shouldBe` Right 54
        it "880086C3E88112" $ (eval <$> parseInput "880086C3E88112") `shouldBe` Right 7
        it "CE00C43D881120" $ (eval <$> parseInput "CE00C43D881120") `shouldBe` Right 9
        it "D8005AC2A8F0" $ (eval <$> parseInput "D8005AC2A8F0") `shouldBe` Right 1
        it "F600BC2D8F" $ (eval <$> parseInput "F600BC2D8F") `shouldBe` Right 0
        it "9C005AC2F8F0" $ (eval <$> parseInput "9C005AC2F8F0") `shouldBe` Right 0
        it "9C0141080250320F1802104A08" $ (eval <$> parseInput "9C0141080250320F1802104A08") `shouldBe` Right 1
