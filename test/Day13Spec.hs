module Day13Spec (
  spec,
) where

import Control.Monad
import Data.Char (intToDigit, isLower)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Vector as Vec
import Day13
import Numeric (showIntAtBase)
import Test.Hspec
import Test.QuickCheck hiding (Small)
import Text.InterpolatedString.QM (qnb)

spec :: Spec
spec =
  describe "day12" $ do
    let inputText =
          [qnb|6,10
               0,14
               9,10
               0,3
               10,4
               4,11
               6,0
               6,12
               4,1
               0,13
               10,12
               3,4
               3,0
               8,4
               1,10
               2,14
               8,10
               9,0

               fold along y=7
               fold along x=5|]

    describe "parseInput" $ do
      describe "example" $ do
        it "parses correcly" $
          parseInput inputText
            `shouldBe` Right
              ( Input
                  { sheet =
                    parseSheet
                      [qnb|...#..#..#.
                           ....#......
                           ...........
                           #..........
                           ...#....#.#
                           ...........
                           ...........
                           ...........
                           ...........
                           ...........
                           .#....#.##.
                           ....#......
                           ......#...#
                           #..........
                           #.#........|]
                  , foldings = [FoldY 7, FoldX 5]
                  }
              )

    describe "part 1" $ do
      describe "example" $ do
        let (Right input) = parseInput inputText
        it "applies first fold" $
          applyfold (FoldY 7) (sheet input)
          `shouldBe`
          parseSheet [qnb|#.##..#..#.
                          #...#......
                          ......#...#
                          #...#......
                          .#.#..#.###
                          ...........
                          ...........|]

parseSheet :: Text -> Sheet
parseSheet input = Set.fromList $ do
  (y,line) <- [0..] `zip` Text.lines input
  (x,c) <- [0..] `zip` Text.unpack line
  guard (c == '#')
  pure (x,y)
