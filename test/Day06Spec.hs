module Day06Spec (
  spec,
) where

import Control.Monad
import Data.Char (intToDigit)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import Day06
import Numeric (showIntAtBase)
import Test.Hspec
import Test.QuickCheck
import Text.InterpolatedString.QM (qnb)
import qualified Data.Vector as Vec

spec :: Spec
spec =
  describe "day05" $ do
    let input = [3,4,3,1,2]

    describe "part 1" $ do
      it "example" $
        sum (gen input 80) `shouldBe` 5934

    describe "part 2" $ do
      it "example" $
        sum (gen input 256) `shouldBe` 26984457539
