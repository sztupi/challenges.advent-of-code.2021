module Day14Spec (
  spec,
) where

import Control.Monad
import Data.Char (intToDigit, isLower)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Vector as Vec
import Day14
import Numeric (showIntAtBase)
import Test.Hspec
import Test.QuickCheck hiding (Small)
import Text.InterpolatedString.QM (qnb)

spec :: Spec
spec =
  describe "day14" $ do
    let inputText =
          [qnb|NNCB

               CH -> B
               HH -> N
               CB -> H
               NH -> C
               HB -> C
               HC -> B
               HN -> C
               NN -> C
               BH -> H
               NC -> B
               NB -> B
               BN -> B
               BB -> N
               BC -> B
               CC -> N
               CN -> C|]

    describe "parseInput" $ do
      describe "example" $ do
        it "parses correcly" $
          parseInput inputText
            `shouldBe` Right
              ( Input
                  "NNCB"
                  (Map.fromList
                   [ (('C', 'H'), 'B')
                   , (('H', 'H'), 'N')
                   , (('C', 'B'), 'H')
                   , (('N', 'H'), 'C')
                   , (('H', 'B'), 'C')
                   , (('H', 'C'), 'B')
                   , (('H', 'N'), 'C')
                   , (('N', 'N'), 'C')
                   , (('B', 'H'), 'H')
                   , (('N', 'C'), 'B')
                   , (('N', 'B'), 'B')
                   , (('B', 'N'), 'B')
                   , (('B', 'B'), 'N')
                   , (('B', 'C'), 'B')
                   , (('C', 'C'), 'N')
                   , (('C', 'N'), 'C')
                   ] ))

    describe "part 1" $ do
      let (Right input@(Input start rules)) = parseInput inputText
      describe "insertion" $ do
        it "example" $
          insertion rules start `shouldBe` "NCNBCHB"

      describe "diffAfter" $ do
        it "gets difference right" $
          diffAfter 10 input `shouldBe` 1588

    describe "part 2" $ do
      let (Right input@(Input start rules)) = parseInput inputText
      describe "insertion" $ do
        it "example" $
          insertionPairs rules (pairs start) `shouldBe` pairs "NCNBCHB"

      describe "diffAfter" $ do
        it "gets difference right" $
          diffAfterPairs 10 input `shouldBe` 1588
