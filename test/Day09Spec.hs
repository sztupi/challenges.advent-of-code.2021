module Day09Spec (
  spec,
) where

import Control.Monad
import Data.Char (intToDigit)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import Day09
import Numeric (showIntAtBase)
import Test.Hspec
import Test.QuickCheck
import Text.InterpolatedString.QM (qnb)
import qualified Data.Vector as Vec

spec :: Spec
spec =
  describe "day09" $ do
    let input = [qnb|2199943210
                     3987894921
                     9856789892
                     8767896789
                     9899965678|]

    describe "part 1" $ do
      describe "lowPoints" $ do
        it "example" $
          (fmap snd . lowPoints . parseInput) input `shouldMatchList` [1, 0, 5, 5]

    describe "part 2" $ do
      describe "basins" $ do
        it "example" $
          (fmap Set.size . basins . parseInput) input `shouldMatchList` [3, 9, 9, 14]
