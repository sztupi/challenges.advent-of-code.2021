module Day03Spec (
  spec,
) where

import Control.Monad
import Data.Char (intToDigit)
import qualified Data.Map.Strict as Map
import qualified Data.Text as Text
import Data.Text (Text)
import Day03
import Numeric (showIntAtBase)
import Test.Hspec
import Test.QuickCheck
import Text.InterpolatedString.QM (qnb)

spec :: Spec
spec =
  describe "day03" $ do
    let input =
          [qnb|00100
                     11110
                     10110
                     10111
                     10101
                     01111
                     00111
                     11100
                     10000
                     11001
                     00010
                     01010
                     |]

    let binaries = parseInput input

    describe "parsing" $ do
      it "simple binary" $
        parseBinary "11001" `shouldBe` Map.fromList [(1, 1), (2, 0), (3, 0), (4, 1), (5, 1)]

    describe "part 1" $ do
      describe "gamma" $ do
        it "example" $ do
          gamma binaries `shouldBe` parseBinary "10110"

      describe "epsilon" $ do
        it "example" $ do
          epsilon binaries `shouldBe` parseBinary "01001"

      it "toInt" $ do
        let showBin :: Int -> Text
            showBin n = Text.pack $ showIntAtBase 2 intToDigit n ""
        property $
          forAll (chooseInt (0, 2 ^ 15)) $ \i ->
            property $ (toInt . parseBinary . showBin) i == i

    describe "part 2" $ do
      describe "oxygen gen rating" $ do
        it "example" $ do
          oxygenRating binaries `shouldBe` parseBinary "10111"

      describe "co2 scrubber rating" $ do
        it "example" $ do
          co2Rating binaries `shouldBe` parseBinary "01010"
