module Day04Spec (
  spec,
) where

import Control.Monad
import Data.Char (intToDigit)
import qualified Data.Map.Strict as Map
import Data.Text (Text)
import qualified Data.Text as Text
import Day04
import Numeric (showIntAtBase)
import Test.Hspec
import Test.QuickCheck
import Text.InterpolatedString.QM (qnb)
import qualified Data.Set as Set

spec :: Spec
spec =
  describe "day04" $ do
    let inputText =
          [qnb|7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

               22 13 17 11  0
                8  2 23  4 24
               21  9 14 16  7
                6 10  3 18  5
                1 12 20 15 19

                3 15  0  2 22
                9 18 13 17  5
               19  8  7 25 23
               20 11 10 24  4
               14 21 16 12  6

               14 21 17 24  4
               10 16 15  9 19
               18  8 23 26 20
               22 11 13  6  5
                2  0 12  3  7
              |]

    describe "parsing" $ do
      it "parses example" $
        parseInput inputText
          `shouldBe` Right
            Input
              { draws = [7, 4, 9, 5, 11, 17, 23, 2, 0, 14, 21, 24, 10, 16, 13, 6, 15, 25, 12, 22, 18, 20, 8, 19, 3, 26, 1]
              , boards =
                  [ mkBoard
                      [ [22, 13, 17, 11, 0]
                      , [8, 2, 23, 4, 24]
                      , [21, 9, 14, 16, 7]
                      , [6, 10, 3, 18, 5]
                      , [1, 12, 20, 15, 19]
                      ]
                  , mkBoard
                      [ [3, 15, 0, 2, 22]
                      , [9, 18, 13, 17, 5]
                      , [19, 8, 7, 25, 23]
                      , [20, 11, 10, 24, 4]
                      , [14, 21, 16, 12, 6]
                      ]
                  , mkBoard
                      [ [14, 21, 17, 24, 4]
                      , [10, 16, 15, 9, 19]
                      , [18, 8, 23, 26, 20]
                      , [22, 11, 13, 6, 5]
                      , [2, 0, 12, 3, 7]
                      ]
                  ]
              }

    let (Right input) = parseInput inputText

    describe "part 1" $ do
      describe "bingos" $ do
        it "generate col and row bingos" $
          bingos
            ( mkBoard
                [ [14, 21, 17, 24, 4]
                , [10, 16, 15, 9, 19]
                , [18, 8, 23, 26, 20]
                , [22, 11, 13, 6, 5]
                , [2, 0, 12, 3, 7]
                ]

            )
            `shouldMatchList` ( Set.fromList
                                  <$> [ [14, 21, 17, 24, 4]
                                      , [10, 16, 15, 9, 19]
                                      , [18, 8, 23, 26, 20]
                                      , [22, 11, 13, 6, 5]
                                      , [2, 0, 12, 3, 7]
                                      , [14, 10, 18, 22, 2]
                                      , [21, 16, 8, 11, 0]
                                      , [17, 15, 23, 13, 12]
                                      , [24, 9, 26, 6, 3]
                                      , [4, 19, 20, 5, 7]
                                      ]
                              )
      it "finds bingo" $
        head (findBingo input) `shouldBe` (24, [ Set.fromList [10, 16, 15, 19, 18, 8, 26, 20, 22, 13, 6, 12, 3] ])

    describe "part 2" $
      it "finds last bingo" $
        last (findBingo input) `shouldBe` (13, [ Set.fromList [3, 15, 22, 18, 19, 8, 25, 20, 12, 6] ])
