module Day25Spec (
  spec,
) where

import Control.Arrow ( second, (&&&), first, (|||), Arrow ((***)) )
import Control.Monad
import Data.Char (intToDigit, isLower)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Vector as Vec
import Day25
import Numeric (showIntAtBase)
import Test.Hspec
import Test.QuickCheck hiding (Small)
import Text.InterpolatedString.QM (qnb)

spec :: Spec
spec =
  describe "day25" $ do
    let exampleInput = [qnb|v...>>.vv>
                            .vv>>.vv..
                            >>.>v>...v
                            >>v>>.>.v.
                            v>v.vv.v..
                            >.>>..v...
                            .vv..>.>v.
                            v.v..>>v.v
                            ....v..v.>
                            |]

    describe "moves" $ do
      it "move 1" $
        let expected = [qnb|....>.>v.>
                            v.v>.>v.v.
                            >v>>..>v..
                            >>v>v>.>.v
                            .>v.v...v.
                            v>>.>vvv..
                            ..v...>>..
                            vv...>>vv.
                            >.v.v..v.v
                           |]
        in moveOne (parseInput exampleInput) `shouldBe` (True, parseInput expected)

        

    describe "part1" $ do
      it "example" $
        (fst . findStable) (parseInput exampleInput) `shouldBe` 58
