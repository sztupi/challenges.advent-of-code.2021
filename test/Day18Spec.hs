module Day18Spec (
  spec,
) where

import Control.Monad
import Data.Char (intToDigit, isLower)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Vector as Vec
import Day18
import Numeric (showIntAtBase)
import Test.Hspec
import Test.QuickCheck hiding (Small)
import Text.InterpolatedString.QM (qnb)

spec :: Spec
spec =
  describe "day18" $ do
    describe "part1" $ do
      describe "explode" $ do
        describe "examples" $ do
          it "1" $
            "[[[[[9,8],1],2],3],4]"
            `shouldExplodeTo`
            "[[[[0,9],2],3],4]"

          it "2" $
            "[7,[6,[5,[4,[3,2]]]]]"
            `shouldExplodeTo`
            "[7,[6,[5,[7,0]]]]"

          it "3" $
            "[[6,[5,[4,[3,2]]]],1]"
            `shouldExplodeTo`
            "[[6,[5,[7,0]]],3]"

          it "4" $
            "[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]"
            `shouldExplodeTo`
            "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]"

          it "5" $
            "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]"
            `shouldExplodeTo`
            "[[3,[2,[8,0]]],[9,[5,[7,0]]]]"

          it "6-1" $
            "[[[[0,[4,5]],[0,0]],[[[4,0],0],0]],0]"
            `shouldExplodeTo`
            "[[[[4,0],[5,0]],[[[4,0],0],0]],0],0]"

          it "6-2" $
            "[[[[4,0],[5,0]],[[[4,0],0],0]],0],0]"
            `shouldExplodeTo`
            "[[[[4,0],[5,4]],[[0,0],0]],0],0]"

      describe "split" $ do
        describe "examples" $ do
          it "1" $
            "[[[[0,7],4],[15,[0,13]]],[1,1]]"
            `shouldSplitTo`
            "[[[[0,7],4],[[7,8],[0,13]]],[1,1]]"

          it "2" $
            "[[[[0,7],4],[[7,8],[0,13]]],[1,1]]"
            `shouldSplitTo`
            "[[[[0,7],4],[[7,8],[0,[6,7]]]],[1,1]]"

      describe "reduce" $ do
        describe "examples" $ do
          it "1" $
            "[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]"
            `shouldReduceTo`
            "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]"

          it "2" $
            "[[[[0,[4,5]],[0,0]],[[[4,0],0],0]],0]"
            `shouldReduceTo`
            "[[[[4,0],[5,4]],[[0,0],0]],0],0]"

      describe "addSnail" $ do
        it "1" $
          ( "[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]"
          , "[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]" )
          `shouldAddTo`
          "[[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]"

        it "2" $
          ( "[[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]"
          , "[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]" )
          `shouldAddTo`
          "[[[[6,7],[6,7]],[[7,7],[0,7]]],[[[8,7],[7,7]],[[8,8],[8,0]]]]"

        it "3" $
          ( "[[[[6,7],[6,7]],[[7,7],[0,7]]],[[[8,7],[7,7]],[[8,8],[8,0]]]]"
          , "[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]" )
          `shouldAddTo`
          "[[[[7,0],[7,7]],[[7,7],[7,8]]],[[[7,7],[8,8]],[[7,7],[8,7]]]]"

        it "4" $
          ( "[[[[7,0],[7,7]],[[7,7],[7,8]]],[[[7,7],[8,8]],[[7,7],[8,7]]]]"
          , "[7,[5,[[3,8],[1,4]]]]" )
          `shouldAddTo`
          "[[[[7,7],[7,8]],[[9,5],[8,7]]],[[[6,8],[0,8]],[[9,9],[9,0]]]]"

        it "5" $
          ( "[[[[7,7],[7,8]],[[9,5],[8,7]]],[[[6,8],[0,8]],[[9,9],[9,0]]]]"
          , "[[2,[2,2]],[8,[8,1]]]" )
          `shouldAddTo`
          "[[[[6,6],[6,6]],[[6,0],[6,7]]],[[[7,7],[8,9]],[8,[8,1]]]]"

        it "6" $
          ( "[[[[6,6],[6,6]],[[6,0],[6,7]]],[[[7,7],[8,9]],[8,[8,1]]]]"
          , "[2,9]" )
          `shouldAddTo`
          "[[[[6,6],[7,7]],[[0,7],[7,7]]],[[[5,5],[5,6]],9]]"

        it "7" $
          ( "[[[[6,6],[7,7]],[[0,7],[7,7]]],[[[5,5],[5,6]],9]]"
          , "[1,[[[9,3],9],[[9,0],[0,7]]]]" )
          `shouldAddTo`
          "[[[[7,8],[6,7]],[[6,8],[0,8]]],[[[7,7],[5,0]],[[5,5],[5,6]]]]"

        it "8" $
          ( "[[[[7,8],[6,7]],[[6,8],[0,8]]],[[[7,7],[5,0]],[[5,5],[5,6]]]]"
          , "[[[5,[7,4]],7],1]" )
          `shouldAddTo`
          "[[[[7,7],[7,7]],[[8,7],[8,7]]],[[[7,0],[7,7]],9]]"

        it "9" $
          ( "[[[[7,7],[7,7]],[[8,7],[8,7]]],[[[7,0],[7,7]],9]]"
          , "[[[[4,2],2],6],[8,7]]" )
          `shouldAddTo`
          "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]"

      describe "homeworkSum" $ do
        it "example1" $
          let (Right input) = parseInput
                [qnb|[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]
                     [7,[[[3,7],[4,3]],[[6,3],[8,8]]]]
                     [[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]
                     [[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]
                     [7,[5,[[3,8],[1,4]]]]
                     [[2,[2,2]],[8,[8,1]]]
                     [2,9]
                     [1,[[[9,3],9],[[9,0],[0,7]]]]
                     [[[5,[7,4]],7],1]
                     [[[[4,2],2],6],[8,7]]
                     |]
              (Right expected) = parseSnail "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]"

          in homeworkSum input `shouldBe` expected

        it "example2" $
          let (Right input) = parseInput
                [qnb|[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
                     [[[5,[2,8]],4],[5,[[9,9],0]]]
                     [6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
                     [[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
                     [[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
                     [[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
                     [[[[5,4],[7,7]],8],[[8,3],8]]
                     [[9,3],[[9,9],[6,[4,9]]]]
                     [[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
                     [[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]
                     |]
              (Right expected) = parseSnail "[[[[6,6],[7,6]],[[7,7],[7,0]]],[[[7,7],[7,7]],[[7,8],[9,9]]]]"

          in homeworkSum input `shouldBe` expected

      describe "largestSum" $ do
        it "example" $
          let (Right input) = parseInput
                [qnb|[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
                     [[[5,[2,8]],4],[5,[[9,9],0]]]
                     [6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
                     [[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
                     [[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
                     [[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
                     [[[[5,4],[7,7]],8],[[8,3],8]]
                     [[9,3],[[9,9],[6,[4,9]]]]
                     [[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
                     [[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]
                     |]
          in largestSum input `shouldBe` 3993

shouldExplodeTo :: Text -> Text -> Expectation
shouldExplodeTo a b =
  let (Right sn) = parseSnail a
      (Right ex) = parseSnail b
  in (fst . explode) sn `shouldBe` ex

shouldSplitTo :: Text -> Text -> Expectation
shouldSplitTo a b =
  let (Right sn) = parseSnail a
      (Right ex) = parseSnail b
  in (fst . split) sn `shouldBe` ex

shouldReduceTo :: Text -> Text -> Expectation
shouldReduceTo a b =
  let (Right sn) = parseSnail a
      (Right ex) = parseSnail b
  in reduce sn `shouldBe` ex

shouldAddTo :: (Text,Text) -> Text -> Expectation
shouldAddTo (a,b) c =
  let (Right a') = parseSnail a
      (Right b') = parseSnail b
      (Right c') = parseSnail c
  in (a' `addSnail` b') `shouldBe` c'
