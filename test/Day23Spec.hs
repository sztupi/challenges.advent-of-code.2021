module Day23Spec (
  spec,
) where

import Control.Arrow ( second, (&&&), first, (|||), Arrow ((***)) )
import Control.Monad
import Data.Char (intToDigit, isLower)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Vector as Vec
import Day23
import Numeric (showIntAtBase)
import Test.Hspec
import Test.QuickCheck hiding (Small)
import Text.InterpolatedString.QM (qnb)
import qualified Debug.Trace as Debug

spec :: Spec
spec =
  describe "day23" $ do
    let start = Text.unlines . fmap (Text.drop 1) . Text.lines
                  $ [qnb||#############
                         |#...........#
                         |###B#C#B#D###
                         |  #A#D#C#A#
                         |  #########|]

    describe "part1" $ do
      describe "debug" $ xit "" $ do
        let (Right res) = fmap (join . fmap (vis . snd) . moves) (parseInput start)
        Debug.trace res $ (res `shouldBe` "")
      it "example 1" $
        fmap solve1 (parseInput start) `shouldBe` Right (Just 12521)

    describe "part2" $ do
      it "example" $
        fmap solve1 (parseInput . injectMissing $ start) `shouldBe` Right (Just 44169)
