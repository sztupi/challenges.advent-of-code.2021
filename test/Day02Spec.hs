module Day02Spec (
  spec,
) where

import Control.Monad
import qualified Data.Text as Text
import Day02
import Test.Hspec
import Text.InterpolatedString.QM (qnb)

spec :: Spec
spec =
  describe "day02" $ do
    let input = [qnb|forward 5
                     down 5
                     forward 8
                     up 3
                     down 8
                     forward 2
                     |]

    describe "parsing" $ do
      it "example" $ do
        parseInstructions input `shouldBe`
          Right [Forward 5, Down 5, Forward 8, Up 3, Down 8, Forward 2]

    describe "part 1 " $ do
      it "example" $ do
        (fmap runInstructions1 . parseInstructions) input `shouldBe` Right (15, 10)

    describe "part 2 " $ do
      it "example" $ do
        (fmap (fst . runInstructions2) . parseInstructions) input `shouldBe` Right (15, 60)
