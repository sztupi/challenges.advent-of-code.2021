module Day20Spec (
  spec,
) where

import Control.Monad
import Data.Char (intToDigit, isLower)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Vector as Vec
import Day20
import Numeric (showIntAtBase)
import Test.Hspec
import Test.QuickCheck hiding (Small)
import Text.InterpolatedString.QM (qnb)

spec :: Spec
spec =
  describe "day20" $ do
    let inputText =
          [qnb|..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#

               #..#.
               #....
               ##..#
               ..#..
               ..###
               |]
            
    describe "part1" $ do
      describe "example" $ do
        let (Right input) = parseInput inputText
        describe "steps" $ do
          it "step 0" $
            let out = parseState [qnb|#..#.
                                      #....
                                      ##..#
                                      ..#..
                                      ..###|]

            in reorient (step 0 input) `shouldMatchSet` reorient out

          it "step 1" $
            let out = parseState [qnb|.##.##.
                                      #..#.#.
                                      ##.#..#
                                      ####..#
                                      .#..##.
                                      ..##..#
                                      ...#.#.|]

            in reorient (step 1 input) `shouldMatchSet` reorient out

          it "step 2" $
            let out = parseState [qnb|.......#.
                                      .#..#.#..
                                      #.#...###
                                      #...##.#.
                                      #.....#.#
                                      .#.#####.
                                      ..#.#####
                                      ...##.##.
                                      ....###..|]

            in reorient (step 2 input) `shouldMatchSet` reorient out

        it "size" $
          (Set.size . step 2) input `shouldBe` 35

    describe "part2" $ do
      describe "example" $ do
        let (Right input) = parseInput inputText
        it "size" $
          (Set.size . step 50) input `shouldBe` 3351

parseState :: Text -> Board
parseState txt = Set.fromList $ do
  (y,line) <- zip [0..] . Text.lines $ txt
  (x,c) <- zip [0..] . Text.unpack $ line
  [ (x,y) | c == '#' ]

shouldMatchSet (Set.toList->a) (Set.toList->b) = a `shouldMatchList` b
