module Day11Spec (
  spec,
) where

import Control.Monad
import Data.Char (intToDigit)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Vector as Vec
import Day11
import Numeric (showIntAtBase)
import Test.Hspec
import Test.QuickCheck
import Text.InterpolatedString.QM (qnb)

spec :: Spec
spec =
  describe "day10" $ do
    let input =
          [qnb|5483143223
                     2745854711
                     5264556173
                     6141336146
                     6357385478
                     4167524645
                     2176841721
                     6882881134
                     4846848554
                     5283751526|]

    describe "part 1" $ do
      describe "example" $ do
        let start = parseInput input
        describe "steps" $ do
          it "step 1" $
            step 1 start
              `shouldBe` ( parseInput
                            [qnb|6594254334
                               3856965822
                               6375667284
                               7252447257
                               7468496589
                               5278635756
                               3287952832
                               7993992245
                               5957959665
                               6394862637|]
                         , 0
                         )

          it "step 2" $
            step 2 start
              `shouldBe` ( parseInput
                            [qnb|8807476555
                                 5089087054
                                 8597889608
                                 8485769600
                                 8700908800
                                 6600088989
                                 6800005943
                                 0000007456
                                 9000000876
                                 8700006848|]
                         , 35
                         )

          it "step 100" $
            step 100 start
              `shouldBe` ( parseInput
                            [qnb|0397666866
                                 0749766918
                                 0053976933
                                 0004297822
                                 0004229892
                                 0053222877
                                 0532222966
                                 9322228966
                                 7922286866
                                 6789998766|]
                         , 1656
                         )

    describe "part 2" $ do
      describe "example" $ do
        let start = parseInput input
        it "findSync" $
          findSync start `shouldBe` 195
