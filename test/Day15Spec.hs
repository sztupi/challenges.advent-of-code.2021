module Day15Spec (
  spec,
) where

import Control.Monad
import Data.Char (intToDigit, isLower)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Vector as Vec
import Day15
import Numeric (showIntAtBase)
import Test.Hspec
import Test.QuickCheck hiding (Small)
import Text.InterpolatedString.QM (qnb)

spec :: Spec
spec =
  describe "day15" $ do
    let inputText =
          [qnb|1163751742
               1381373672
               2136511328
               3694931569
               7463417111
               1319128137
               1359912421
               3125421639
               1293138521
               2311944581|]

    describe "part 1" $ do
      let cave = parseInput inputText

      describe "cheapestPath" $ do
        xit "example" $
          cheapestPath cave `shouldBe` Nothing

      describe "cheapestPathRisk" $ do
        it "example" $
          cheapestPathRisk cave `shouldBe` Just 40

    describe "part 2" $ do
      let cave = parseInput inputText

      describe "cheapestPathRisk * 5" $ do
        it "example" $
          cheapestPathRisk (upsizeCave 5 cave) `shouldBe` Just 315
