module Day01Spec (
  spec,
) where

import qualified Data.Text as Text
import Day01
import Test.Hspec

spec :: Spec
spec =
  describe "day01" $ do
    let input = [199, 200, 208, 210, 200, 207, 240, 269, 260, 263]
    describe "part 1" $ do
      it "example" $
        countIncreases input `shouldBe` 7
    describe "part 2" $ do
      it "example" $
        windowSumIncreases 3 input `shouldBe` 5
