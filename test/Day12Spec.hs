module Day12Spec (
  spec,
) where

import Control.Monad
import Data.Char (intToDigit, isLower)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Vector as Vec
import Day12
import Numeric (showIntAtBase)
import Test.Hspec
import Test.QuickCheck hiding (Small)
import Text.InterpolatedString.QM (qnb)

spec :: Spec
spec =
  describe "day12" $ do
    let inputSmall =
          [qnb|start-A
               start-b
               A-c
               A-b
               b-d
               A-end
               b-end|]

    describe "parseInput" $ do
      describe "example" $ do
        it "parses correcly" $
          parseInput inputSmall
            `shouldBe` Right
              ( fromEdges
                  [ (Start, Big "A")
                  , (Start, Small "b")
                  , (Big "A", Small "c")
                  , (Big "A", Small "b")
                  , (Small "b", Small "d")
                  , (Big "A", End)
                  , (Small "b", End)
                  ]
              )

    describe "part 1" $ do
      describe "examples" $ do
        describe "small" $ do
          let (Right cave) = parseInput inputSmall
          it "finds all paths" $
            paths eachNonBigOnce cave `shouldMatchList`
              [ [ Start, Big "A", Small "b", Big "A", Small "c", Big "A", End ]
              , [ Start, Big "A", Small "b", Big "A", End ]
              , [ Start, Big "A", Small "b", End ]
              , [ Start, Big "A", Small "c", Big "A", Small "b", Big "A", End ]
              , [ Start, Big "A", Small "c", Big "A", Small "b", End ]
              , [ Start, Big "A", Small "c", Big "A", End ]
              , [ Start, Big "A", End ]
              , [ Start, Small "b", Big "A", Small "c", Big "A", End ]
              , [ Start, Small "b", Big "A", End ]
              , [ Start, Small "b", End ]
              ]

    describe "part 2" $ do
      describe "examples" $ do
        describe "small" $ do
          let (Right cave) = parseInput inputSmall
          it "finds all paths" $
            paths atMostOneSmallTwice cave `shouldMatchList`
              parseSolutions [qnb|start,A,b,A,b,A,c,A,end
                                  start,A,b,A,b,A,end
                                  start,A,b,A,b,end
                                  start,A,b,A,c,A,b,A,end
                                  start,A,b,A,c,A,b,end
                                  start,A,b,A,c,A,c,A,end
                                  start,A,b,A,c,A,end
                                  start,A,b,A,end
                                  start,A,b,d,b,A,c,A,end
                                  start,A,b,d,b,A,end
                                  start,A,b,d,b,end
                                  start,A,b,end
                                  start,A,c,A,b,A,b,A,end
                                  start,A,c,A,b,A,b,end
                                  start,A,c,A,b,A,c,A,end
                                  start,A,c,A,b,A,end
                                  start,A,c,A,b,d,b,A,end
                                  start,A,c,A,b,d,b,end
                                  start,A,c,A,b,end
                                  start,A,c,A,c,A,b,A,end
                                  start,A,c,A,c,A,b,end
                                  start,A,c,A,c,A,end
                                  start,A,c,A,end
                                  start,A,end
                                  start,b,A,b,A,c,A,end
                                  start,b,A,b,A,end
                                  start,b,A,b,end
                                  start,b,A,c,A,b,A,end
                                  start,b,A,c,A,b,end
                                  start,b,A,c,A,c,A,end
                                  start,b,A,c,A,end
                                  start,b,A,end
                                  start,b,d,b,A,c,A,end
                                  start,b,d,b,A,end
                                  start,b,d,b,end
                                  start,b,end|]

parseSolution :: Text -> [Node]
parseSolution =
  fmap (\case
        "start" -> Start
        "end" -> End
        str -> if isLower (Text.head str) then Small str else Big str
    )
  . Text.splitOn ","

parseSolutions :: Text -> [[Node]]
parseSolutions = fmap parseSolution . Text.lines
