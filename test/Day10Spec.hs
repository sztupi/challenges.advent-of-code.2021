module Day10Spec (
  spec,
) where

import Control.Monad
import Data.Char (intToDigit)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import Day10
import Numeric (showIntAtBase)
import Test.Hspec
import Test.QuickCheck
import Text.InterpolatedString.QM (qnb)
import qualified Data.Vector as Vec

spec :: Spec
spec =
  describe "day10" $ do
    let input = [qnb|
                    [({(<(())[]>[[{[]{<()<>>
                    [(()[<>])]({[<{<<[]>>(
                    {([(<{}[<>[]}>{[]{[(<()>
                    (((({<>}<{<{<>}{[]{[]{}
                    [[<[([]))<([[{}[[()]]]
                    [{[{({}]{}}([{[{{{}}([]
                    {<[[]]>}<{[{[{[]{()[[[]
                    [<(<(<(<{}))><([]([]()
                    <{([([[(<>()){}]>(<<{{
                    <{([{{}}[<[[[<>{}]]]>[]]
                    |]

    describe "part 1" $ do
      describe "corrupts" $ do
        it "example" $
          ((\case Corrupt _ c -> [c]; _ -> []) <=< parseInput) input `shouldMatchList` ['}', ')', ']', ')', '>']

    describe "part 2" $ do
      describe "scoreIncomplete" $ do
        it "example" $
          (filter (/= 0) . fmap scoreIncomplete . parseInput) input `shouldMatchList`
            [294,995444,1480781,5566,288957]
