module Day17Spec (
  spec,
) where

import Control.Monad
import Data.Char (intToDigit, isLower)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Vector as Vec
import Day17
import Numeric (showIntAtBase)
import Test.Hspec
import Test.QuickCheck hiding (Small)
import Text.InterpolatedString.QM (qnb)

spec :: Spec
spec =
  describe "day17" $ do
    let input = "target area: x=20..30, y=-10..-5"
    describe "part 1" $ do
      it "parsing" $
        parseInput input `shouldBe` Right ((20,30),(-10,-5))

      it "example" $
        (highest <$> parseInput input) `shouldBe` Right 45

    describe "part 2" $ do
      it "example" $
        (length . findAllHits <$> parseInput input) `shouldBe` Right 112
