module Day07Spec (
  spec,
) where

import Control.Monad
import Data.Char (intToDigit)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import Day07
import Numeric (showIntAtBase)
import Test.Hspec
import Test.QuickCheck
import Text.InterpolatedString.QM (qnb)
import qualified Data.Vector as Vec

spec :: Spec
spec =
  describe "day07" $ do
    let input = [16,1,2,0,4,2,7,1,2,14]

    describe "part 1" $ do
      it "example" $
        leastDistanceSum input `shouldBe` (2,37)

    describe "part 2" $ do
      it "example" $
        leastDistanceIncSum input `shouldBe` (5,168)
