module Day05Spec (
  spec,
) where

import Control.Monad
import Data.Char (intToDigit)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import Day05
import Numeric (showIntAtBase)
import Test.Hspec
import Test.QuickCheck
import Text.InterpolatedString.QM (qnb)

spec :: Spec
spec =
  describe "day05" $ do
    let input =
          [qnb|0,9 -> 5,9
               8,0 -> 0,8
               9,4 -> 3,4
               2,2 -> 2,1
               7,0 -> 7,4
               6,4 -> 2,0
               0,9 -> 2,9
               3,4 -> 1,4
               0,0 -> 8,8
               5,5 -> 8,2|]

    describe "parsing" $ do
      it "parse example" $
        parseInput input
          `shouldBe` Right
            [ ((0, 9), (5, 9))
            , ((8, 0), (0, 8))
            , ((9, 4), (3, 4))
            , ((2, 2), (2, 1))
            , ((7, 0), (7, 4))
            , ((6, 4), (2, 0))
            , ((0, 9), (2, 9))
            , ((3, 4), (1, 4))
            , ((0, 0), (8, 8))
            , ((5, 5), (8, 2))
            ]

    describe "part 1" $ do
      it "example" $
        (countOverlap . ventMapHL <$> parseInput input) `shouldBe` Right 5

    describe "part 2" $ do
      it "example" $
        (countOverlap . ventMapHLD <$> parseInput input) `shouldBe` Right 12
