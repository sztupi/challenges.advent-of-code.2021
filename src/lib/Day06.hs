module Day06 (
  day,
  gen,
  parseInput,
) where

import Control.Arrow ((&&&), second)
import Control.Monad
import Data.Char (digitToInt)
import Data.Either
import Data.Either.Extra
import Data.Function (on)
import Data.List (sortBy)
import qualified Data.List as List
import Data.Map (Map)
import Data.Set (Set)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.Maybe (fromMaybe, listToMaybe)
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Read as Text
import Data.Void (Void)
import Day (Day (Day))
import Text.Megaparsec hiding (Pos)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer hiding (space)
import Data.Vector (Vector, (!))
import qualified Data.Vector as Vec

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . sum . (`gen` 80) . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . sum . (`gen` 256) . parseInput

parseInput :: Text -> [Int]
parseInput = fmap fst . rights . fmap Text.decimal . Text.splitOn ","

gen :: [Int] -> Int -> Vector Int
gen input = (fmap s [0 ..] !!)
  where
    s 0 = Vec.fromList $ fmap (\i -> length $ filter (== i) input) [0..8]
    s n = let p = s (n-1)
          in Vec.fromList [ p ! 1
                          , p ! 2
                          , p ! 3
                          , p ! 4
                          , p ! 5
                          , p ! 6
                          , p ! 7 + p ! 0
                          , p ! 8
                          , p ! 0
                          ]
