module Day02 (
  day,
  Instruction (..),
  parseInstructions,
  runInstructions1,
  runInstructions2
) where

import Control.Monad
import Data.Either
import Data.Either.Extra
import Data.List
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Read as Text
import Data.Void (Void)
import Day (Day (Day))
import Text.Megaparsec hiding (Pos)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer hiding (space)

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . fmap (uncurry (*) . runInstructions1) . parseInstructions

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . fmap (uncurry (*) . fst . runInstructions2) . parseInstructions

data Instruction
  = Forward Int
  | Down Int
  | Up Int
  deriving (Show, Eq)

type Pos = (Int, Int)

runInstructions1 :: [Instruction] -> Pos
runInstructions1 = foldl runInstruction1 (0, 0)

runInstruction1 :: Pos -> Instruction -> Pos
runInstruction1 (x, y) = \case
  (Forward n) -> (x + n, y)
  (Down n) -> (x, y + n)
  (Up n) -> (x, y - n)

type Aim = Int

runInstructions2 :: [Instruction] -> (Pos, Aim)
runInstructions2 = foldl runInstruction2 ((0, 0), 0)

runInstruction2 :: (Pos, Aim) -> Instruction -> (Pos,Aim)
runInstruction2 ((x, y), a) = \case
  (Forward n) -> ((x+n,y+a*n),a)
  (Down n) -> ((x,y),a+n)
  (Up n) -> ((x,y),a-n)

type P = Parsec Void Text

parseInstructions :: Text -> Either Text [Instruction]
parseInstructions = mapLeft (Text.pack . show) . runParser instructionsP "<none>"

instructionsP :: P [Instruction]
instructionsP = instructionP `sepEndBy` newline

{- ORMOLU_DISABLE -}
instructionP :: P Instruction
instructionP =
  choice
    [ Forward <$> (string "forward " *> signed space decimal)
    , Down    <$> (string "down "    *> signed space decimal)
    , Up      <$> (string "up "      *> signed space decimal)
    ]
