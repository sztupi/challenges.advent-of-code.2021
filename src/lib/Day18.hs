module Day18 (
  day,
  parseInput,
  parseSnail,
  explode,
  split,
  reduce,
  Snail(..),
  addSnail,
  magnitude,
  homeworkSum,
  largestSum,
) where

import Control.Arrow ( second, (&&&), first, (|||), Arrow ((***)) )
import Control.Monad (guard, join)
import Data.Either.Extra (mapLeft)
import Data.Map (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe ( fromMaybe, mapMaybe )
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Read as Text
import Data.Tuple (swap)
import Data.Void (Void)
import Day (Day (Day))
import qualified Data.Maybe as Maybe
import qualified Debug.Trace as Debug
import qualified Data.List as List
import qualified Data.List.Extra as List
import qualified Data.Char as Char
import qualified Data.PQueue.Min as PQ
import Data.Bifunctor (Bifunctor(bimap))
import Control.Applicative (Alternative(..))
import Control.Monad.Extra (MonadPlus)
import qualified Data.Bits as Bits
import Text.Megaparsec hiding (Pos)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer hiding (space)

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . fmap (magnitude . homeworkSum) . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . fmap largestSum . parseInput

type Input = [Snail]

data Snail
  = N Int
  | P Snail Snail
  deriving (Show, Eq, Ord)

explode :: Snail -> (Snail, Bool)
explode = second Maybe.isJust . explode' 0
  where
    explode' :: Int -> Snail -> (Snail, Maybe (Maybe Int, Maybe Int))
    explode' _ s@(N _) = (s, Nothing)
    explode' n (P (N ln) (N rn))
      | n >= 4 = (N 0, Just (Just ln, Just rn))
    explode' n (P l r) =
        case explode' (n+1) l of
          (l', Just (ln,rn)) ->
            let (r', rn') = addLeft r rn
            in (P l' r', Just (ln, rn'))
          (_, Nothing) ->
            case explode' (n+1) r of
              (_, Nothing) -> (P l r, Nothing)
              (r', Just (ln,rn)) ->
                let (l', ln') = addRight l ln
                in (P l' r', Just (ln', rn))
    explode _ _ = error "invalid snailnum"

    addLeft, addRight :: Snail -> Maybe Int -> (Snail, Maybe Int)
    addLeft s Nothing = (s, Nothing)
    addLeft (N n) (Just m) = (N (n+m), Nothing)
    addLeft (P l r) ln =
      let (l', ln') = addLeft l ln
      in (P l' r, ln')

    addRight s Nothing = (s, Nothing)
    addRight (N n) (Just m) = (N (n+m), Nothing)
    addRight (P l r) rn =
      let (r', rn') = addRight r rn
      in (P l r', rn')

split :: Snail -> (Snail, Bool)
split s@(N n) | n < 10 = (s, False)
split s@(N n) = (P (N $ n `div` 2) (N $ n `div` 2 + n `mod` 2), True)
split (P l r) =
  case split l of
    (l', True) -> (P l' r, True)
    (_, False) ->
      case split r of
        (r', True) -> (P l r', True)
        (_, False) -> (P l r, False)

reduce :: Snail -> Snail
reduce s =
  case explode s of
    (_, False) ->
      case split s of
        (_, False) -> s
        (s', True) -> reduce s'
    (s', True) -> reduce s'

magnitude :: Snail -> Int
magnitude (N n) = n
magnitude (P l r) = 3 * magnitude l + 2 * magnitude r

addSnail :: Snail -> Snail -> Snail
addSnail l r = reduce (P l r)

homeworkSum :: Input -> Snail
homeworkSum = List.foldl1' addSnail

largestSum :: [Snail] -> Int
largestSum nums = maximum $ do
  (a,b) <- [ (a,b) | a <- nums, b <- nums, a /= b ]
  [ magnitude (a `addSnail` b), magnitude (b `addSnail` a) ]

type P = Parsec Void Text

parseInput :: Text -> Either Text Input
parseInput = mapLeft (Text.pack . show) . runParser inputP ""

parseSnail :: Text -> Either Text Snail
parseSnail = mapLeft (Text.pack . show) . runParser snailP ""

inputP :: P Input
inputP = snailP `sepEndBy` newline

snailP :: P Snail
snailP = choice [ between (char '[') (char ']') (P <$> snailP <*> (char ',' *> snailP))
                , N <$> decimal
                ]
