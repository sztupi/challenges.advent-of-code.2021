module Day21 (
  day,
  parseInput,
  runGame,
  runGame2
) where

import Control.Arrow ( second, (&&&), first, (|||), Arrow ((***)) )
import Control.Monad (guard, join, (<=<))
import Data.Either.Extra (mapLeft)
import Data.Map (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe ( fromMaybe, mapMaybe )
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Read as Text
import Data.Tuple (swap)
import Data.Void (Void)
import Day (Day (Day))
import qualified Data.Maybe as Maybe
import qualified Debug.Trace as Debug
import qualified Data.List as List
import qualified Data.List.Extra as List
import qualified Data.Char as Char
import qualified Data.PQueue.Min as PQ
import Data.Bifunctor (Bifunctor(bimap))
import Control.Monad.Extra (MonadPlus)
import qualified Data.Bits as Bits
import Text.Megaparsec hiding (Pos, match)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer hiding (space)
import Data.Functor (($>))
import Data.MemoTrie (memo)

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . fmap (uncurry (*) . runGame [1..]) . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . fmap (uncurry max . runGame2) . parseInput

runGame :: [Int] -> Input -> (Int, Int)
runGame dice (start1,start2) = runGame' 0 dice start1 start2 0 0
  where
    runGame' :: Int -> [Int] -> Int -> Int -> Int -> Int -> (Int, Int)
    runGame' d _ _ _ s1 s2 | s2 >= 1000 = (s1, d)
    runGame' d (d1:d2:d3:ds) p1 p2 s1 s2 =
      let p1' = (p1 + d1 + d2 + d3 - 1) `mod` 10 + 1
          s1' = s1+p1'
      in runGame' (d+3) ds p2 p1' s2 s1'

    runGame' _ _ _ _ _ _ = error "invalid input"

throws :: [(Int, Integer)]
throws = Map.toList . Map.fromListWith (+) $ do
  d1 <- [1..3]
  d2 <- [1..3]
  d3 <- [1..3]
  pure (d1+d2+d3, 1)

data Turn = P1 | P2
  deriving (Show, Eq, Ord)

runGame2 :: Input -> (Integer, Integer)
runGame2 (st1, st2) = process (0,0) (Map.singleton (P1, ((st1,0),(st2,0))) 1)
  where
    process :: (Integer, Integer) -> Map (Turn, ((Int,Int),(Int,Int))) Integer -> (Integer, Integer)
    process wins (Map.minViewWithKey -> Nothing) = wins
    process (w1,w2) (Map.minViewWithKey -> Just (((P2, ((_,s1),_)), mul),ts)) | s1 >= 21
      = process (w1+mul,w2) ts
    process (w1,w2) (Map.minViewWithKey -> Just (((P1, (_,(_,s2))), mul),ts)) | s2 >= 21
      = process (w1,w2+mul) ts
    process (w1,w2) (Map.minViewWithKey -> Just (((P1, ((p1,s1),(p2,s2))), mul),ts)) =
      let next = do
            (t,m) <- throws
            let p' = (p1 + t - 1) `mod` 10 + 1
                s' = s1+p'
            pure $ Map.insertWith (+) (P2, ((p', s'),(p2,s2))) (mul*m)
      in process (w1,w2) (List.foldl' (.) id next ts)

    process (w1,w2) (Map.minViewWithKey -> Just (((P2, ((p1,s1),(p2,s2))), mul),ts)) =
      let next = do
            (t,m) <- throws
            let p' = (p2 + t - 1) `mod` 10 + 1
                s' = s2+p'
            pure $ Map.insertWith (+) (P1, ((p1,s1),(p', s'))) (mul*m)
      in process (w1,w2) (List.foldl' (.) id next ts)

type Input = (Int,Int)

type P = Parsec Void Text

parseInput :: Text -> Either Text Input
parseInput = mapLeft (Text.pack . show) . runParser inputP ""

inputP :: P Input
inputP = (,) <$> (posP <* newline) <*> posP

posP :: P Int
posP = string "Player " *> decimal *> string " starting position: " *> decimal
