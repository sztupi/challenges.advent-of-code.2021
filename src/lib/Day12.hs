module Day12 (
  day,
  parseInput,
  Node(..),
  Cave(..),
  fromEdges,
  paths,
  eachNonBigOnce,
  atMostOneSmallTwice,
) where

import Data.Map (Map)
import Data.Set (Set)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Read as Text
import Day (Day (Day))
import Text.Megaparsec hiding (Pos)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer hiding (space)
import Data.Void (Void)
import Data.Either.Extra (mapLeft)
import Control.Arrow (second, (&&&))
import Data.Tuple (swap)
import Control.Monad (join, guard)
import Data.Maybe (fromMaybe)
import qualified Debug.Trace as Debug

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . fmap (length . paths eachNonBigOnce) . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . fmap (length . paths atMostOneSmallTwice) . parseInput

data Node
  = Start
  | End
  | Small Text
  | Big Text
  deriving (Eq, Ord, Show)

newtype Cave = Cave { edges :: Map Node (Set Node) }
  deriving (Eq, Ord, Show)

fromEdges :: [(Node,Node)] -> Cave
fromEdges = Cave . Map.fromListWith Set.union . fmap (second Set.singleton) . uncurry (++) . (id &&& fmap swap)

nodes :: Cave -> [Node]
nodes = Map.keys . edges

edgesFrom :: Cave -> Node -> Set Node
edgesFrom cave = fromMaybe Set.empty . flip Map.lookup (edges cave)

type Path = [Node]

isSmall :: Node -> Bool
isSmall (Small _) = True
isSmall _ = False

isBig :: Node -> Bool
isBig (Big _) = True
isBig _ = False

eachNonBigOnce :: Path -> Bool
eachNonBigOnce =
  all ((== 1) . snd)
  . filter (not . isBig . fst)
  . Map.toList
  . Map.fromListWith (+)
  . fmap (,1)

atMostOneSmallTwice :: Path -> Bool
atMostOneSmallTwice path =
  let nonBigs = filter (not . isBig . fst) . Map.toList . Map.fromListWith (+) . fmap (,1) $ path
      moreThanOnces = filter ((> 1) . snd) nonBigs
  in  all ((<= 2) . snd) moreThanOnces && (length moreThanOnces <= 1) && all (isSmall . fst) moreThanOnces

paths :: (Path -> Bool) -> Cave -> [Path]
paths isPathValid cave = reverse <$> paths' Start []
  where
    paths' :: Node -> Path -> [Path]
    paths' End path = [ End:path ]
    paths' node path = join $ do
      let path' = node:path
      guard $ isPathValid path'
      next <- Set.toList $ edgesFrom cave node
      return $ paths' next path'

type P = Parsec Void Text

parseInput :: Text -> Either Text Cave
parseInput = mapLeft (Text.pack . show) . runParser inputP "<none>"

inputP :: P Cave
inputP = fromEdges <$> (lineP `sepEndBy` newline)

lineP :: P (Node,Node)
lineP = (,) <$> (nodeP <* char '-') <*> nodeP

nodeP :: P Node
nodeP = choice
  [ Start <$ string "start"
  , End <$ string "end"
  , Big . Text.pack <$> some upperChar
  , Small . Text.pack <$> some lowerChar
  ]
