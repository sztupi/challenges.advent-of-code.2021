module Day16 (
  day,
  parseInput,
  Packet(..),
  PData(..),
  eval
) where

import Control.Arrow (second, (&&&))
import Control.Monad (guard, join)
import Data.Either.Extra (mapLeft)
import Data.Map (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe ( fromMaybe, mapMaybe )
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Read as Text
import Data.Tuple (swap)
import Data.Void (Void)
import Day (Day (Day))
import qualified Data.Maybe as Maybe
import qualified Debug.Trace as Debug
import qualified Data.List as List
import qualified Data.List.Extra as List
import qualified Data.Char as Char
import qualified Data.PQueue.Min as PQ
import Data.Bifunctor (Bifunctor(bimap))
import Control.Applicative (Alternative(..))
import Control.Monad.Extra (MonadPlus)
import Control.Arrow (first)
import qualified Data.Bits as Bits
import Control.Monad.Combinators

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . fmap vsum . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . fmap eval . parseInput

type Input = Packet

data Packet
  = Packet
  { pversion :: Int
  , pdata :: PData
  } deriving (Show, Eq)

data PData
  = PLiteral Int
  | POperator Int [Packet]
  deriving (Show, Eq)

vsum :: Packet -> Int
vsum p = pversion p + pdsum (pdata p)

pdsum :: PData -> Int
pdsum (PLiteral _) = 0
pdsum (POperator _ ps) = sum $ vsum <$> ps

eval :: Packet -> Int
eval = evalData . pdata

evalData :: PData -> Int
evalData (PLiteral l) = l
evalData (POperator 0 ps) = sum $ eval <$> ps
evalData (POperator 1 ps) = product $ eval <$> ps
evalData (POperator 2 ps) = minimum $ eval <$> ps
evalData (POperator 3 ps) = maximum $ eval <$> ps
evalData (POperator 5 [p1,p2]) = if eval p1 > eval p2 then 1 else 0
evalData (POperator 6 [p1,p2]) = if eval p1 < eval p2 then 1 else 0
evalData (POperator 7 [p1,p2]) = if eval p1 == eval p2 then 1 else 0
evalData _ = error "invalid input"


type Bit = Bool

newtype P a = P { runP :: [Bit] -> Either Text ([Bit],a) }
  -- deriving (Functor, Applicative, Alternative, Monad, MonadPlus)

instance Functor P where
  fmap f p = P $ fmap (second f) . runP p

instance Applicative P where
  pure a = P $ \bits -> Right (bits, a)

  fp <*> p = P $ \bits -> do
    (bits',f) <- runP fp bits
    second f <$> runP p bits'

instance Alternative P where
  empty = P $ const $ Left "no alternatives"
  p1 <|> p2 = P $ \bits -> case runP p1 bits of
    Right res -> pure res
    Left e1 -> runP p2 bits

instance Monad P where
  -- p :: P a -- k :: a -> P b
  p >>= k = P $ \bits -> do
    (bits', a) <- runP p bits
    runP (k a) bits'

instance MonadPlus P

instance MonadFail P where
  fail = P . const . Left . Text.pack

bitP :: P Bit
bitP = P $ \case [] -> Left "no more input"
                 (b:bs) -> Right (bs, b)

eos :: P ()
eos = P $ \case [] -> Right ([], ())
                _ -> Left "expected eos"

toBits :: Text -> [Bit]
toBits =
  join
  . fmap (\n -> [ b | i <- [3,2,1,0], let b = Bits.testBit n i ])
  . fmap Char.digitToInt
  . Text.unpack

toInt :: [Bit] -> Int
toInt = List.foldl' (\s b -> 2*s + if b then 1 else 0) 0

parseInput :: Text -> Either Text Input
parseInput = fmap snd . runP inputP . toBits

inputP :: P Packet
inputP = packetP

packetP :: P Packet
packetP = do
  pversion <- toInt <$> count 3 bitP
  pdata <- dataP
  pure Packet{..}

packetsP :: P [Packet]
packetsP =
  choice [ [] <$ eos
         , (:) <$> packetP <*> packetsP
         ]

dataP :: P PData
dataP = do
    typ <- toInt <$> count 3 bitP
    case typ of
      4 -> literalP
      _ -> operatorP typ

literalP :: P PData
literalP = PLiteral . toInt <$> listP
  where
    listP :: P [Bit]
    listP = do
      c <- bitP
      bits <- count 4 bitP
      if c
        then do
          bits' <- listP
          pure $ bits <> bits'
        else
          pure bits

operatorP :: Int -> P PData
operatorP typ = do
  ltype <- bitP
  POperator typ <$> (
    case ltype of
      False -> do
        l <- toInt <$> count 15 bitP
        bits <- count l bitP
        case runP packetsP bits of
          Right (_,ps) -> pure ps
          Left err -> fail (Text.unpack err)

      True -> do
        l <- toInt <$> count 11 bitP
        count l packetP
    )
