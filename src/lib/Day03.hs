module Day03 (
  day,
  parseInput,
  parseBinary,
  Digits,
  gamma,
  epsilon,
  toInt,
  oxygenRating,
  co2Rating
) where

import Control.Arrow ((&&&))
import Control.Monad
import Data.Char (digitToInt)
import Data.Either
import Data.Either.Extra
import Data.Function (on)
import Data.List (sortBy)
import qualified Data.List as List
import Data.Map (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe (fromMaybe)
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Read as Text
import Data.Void (Void)
import Day (Day (Day))
import Text.Megaparsec hiding (Pos)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer hiding (space)

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . uncurry (*) . (toInt . gamma &&& toInt . epsilon) . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . uncurry (*) . (toInt . oxygenRating &&& toInt . co2Rating) . parseInput

type Digits = Map Int Int

parseInput :: Text -> [Digits]
parseInput = fmap parseBinary . Text.lines

parseBinary :: Text -> Digits
parseBinary = Map.fromList . zip [1 ..] . fmap digitToInt . reverse . Text.unpack

type Frequency = Map Int Int

frequencies :: [Digits] -> Map Int Frequency
frequencies =
  foldl countDigits Map.empty
    . fmap (fmap (`Map.singleton` 1))
 where
  countDigits :: Map Int Frequency -> Map Int Frequency -> Map Int Frequency
  countDigits = Map.unionWith (Map.unionWith (+))

gamma :: [Digits] -> Digits
gamma = fmap mostFrequent . frequencies
 where
  mostFrequent :: Frequency -> Int
  mostFrequent = fst . head . List.sortOn ((* (-1)) . snd) . Map.toList

epsilon :: [Digits] -> Digits
epsilon = fmap leastFrequent . frequencies
 where
  leastFrequent :: Frequency -> Int
  leastFrequent = fst . head . List.sortOn snd . Map.toList

toInt :: Digits -> Int
toInt = sum . fmap (\(a, b) -> b * 2 ^ (a -1)) . Map.toList

type BitCriterion = Int -> [Digits] -> Digits -> Bool

mostCommon :: BitCriterion
mostCommon n ds =
  let freqs = frequencies ds
      matchValue = fromMaybe 0 $ do
        nthFreq <- Map.lookup n freqs
        ones <- Map.lookup 1 nthFreq
        zeroes <- Map.lookup 0 nthFreq
        pure $ if ones >= zeroes then 1 else 0
   in (Just matchValue ==) . Map.lookup n

leastCommon :: BitCriterion
leastCommon n ds =
  let freqs = frequencies ds
      matchValue = fromMaybe 0 $ do
        nthFreq <- Map.lookup n freqs
        ones <- Map.lookup 1 nthFreq
        zeroes <- Map.lookup 0 nthFreq
        pure $ if ones >= zeroes then 0 else 1
   in (Just matchValue ==) . Map.lookup n

findRating :: BitCriterion -> [Digits] -> Digits
findRating crit ds = step (length . head $ ds) ds
 where
  step :: Int -> [Digits] -> Digits
  step _ [] = error "shouldn't be with these inputs"
  step _ [d] = d
  step n ds =
    let match = crit n ds
     in step (n - 1) (filter match ds)

oxygenRating, co2Rating :: [Digits] -> Digits
oxygenRating = findRating mostCommon
co2Rating = findRating leastCommon
