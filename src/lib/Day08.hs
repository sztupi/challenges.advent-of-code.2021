module Day08 (
  day,
  parseInput,
  InputLine(..),
  countUniqueLengthOutput,
  findSegmentMap,
  readOutput
) where

import Control.Arrow ((&&&), second)
import Control.Monad
import Data.Char (digitToInt, ord)
import Data.Either
import Data.Either.Extra
import Data.Function (on)
import Data.List (sortBy)
import qualified Data.List as List
import Data.Map (Map)
import Data.Set (Set)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.Maybe (fromMaybe, listToMaybe)
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Read as Text
import Data.Void (Void)
import Day (Day (Day))
import Text.Megaparsec hiding (Pos)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer hiding (space)
import Data.Vector (Vector, (!))
import qualified Data.Vector as Vec
import qualified Data.Tuple as Tuple

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . fmap countUniqueLengthOutput . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . fmap (fmap sum . mapM readOutput) . parseInput

type Input = [InputLine]

type Segment = Char

data InputLine = InputLine { patterns :: [Set Segment], outputs :: [Set Segment] }
  deriving (Show, Eq)

countUniqueLengthOutput :: Input -> Int
countUniqueLengthOutput = length . filter (`elem` [2, 3, 4, 7]) . join . fmap (fmap Set.size . outputs)

numberSegments :: Map (Set Segment) Int
numberSegments = Map.fromList $ Tuple.swap . second Set.fromList <$>
  [ (0, "abcefg")
  , (1, "cf")
  , (2, "acdeg")
  , (3, "acdfg")
  , (4, "bcdf")
  , (5, "abdfg")
  , (6, "abdefg")
  , (7, "acf")
  , (8, "abcdefg")
  , (9, "abcdfg")
  ]

type SegmentMap = [Segment]

applyMap :: SegmentMap -> Segment -> Segment
applyMap smap input = "abcdefg" !! fromMaybe 0 (List.elemIndex input smap)

translate :: SegmentMap -> Set Segment -> Set Segment
translate smap = Set.fromList . fmap (applyMap smap) . Set.toList

findSegmentMap :: [Set Segment] -> Maybe SegmentMap
findSegmentMap patterns = listToMaybe $ do
    smap <- List.permutations "abcdefg"
    guard $ isSolution smap
    return smap
  where
    isSolution :: SegmentMap -> Bool
    isSolution smap = isSolution' patterns numberSegments
      where
        isSolution' :: [Set Segment] -> Map (Set Segment) Int -> Bool
        isSolution' [] _ = True
        isSolution' (ptn:ptns) segs =
          let mapped = translate smap ptn
          in case Map.lookup mapped segs of
            Nothing -> False
            Just _n -> isSolution' ptns (Map.delete mapped segs)

readOutput :: InputLine -> Maybe Int
readOutput InputLine{patterns, outputs} = do
  smap <- findSegmentMap patterns
  digits <- forM outputs $ \output -> Map.lookup (translate smap output) numberSegments
  return $ List.foldl' (\s d -> s*10+d) 0 digits

type P = Parsec Void Text

parseInput :: Text -> Either Text Input
parseInput = mapLeft (Text.pack . show) . runParser inputP "<none>"

inputP :: P Input
inputP = lineP `sepEndBy` newline

lineP :: P InputLine
lineP = InputLine <$> (segmentsP `sepEndBy` hspace1) <*> (string "| " *> segmentsP `sepBy` hspace1)

segmentsP :: P (Set Segment)
segmentsP = Set.fromList <$> some (oneOf ['a'..'g'])
