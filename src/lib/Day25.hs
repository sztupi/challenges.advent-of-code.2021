module Day25 (
  day,
  parseInput,
  findStable,
  findStable',
  moveOne,
  Seabed(..)
) where

import Control.Arrow ( second, (&&&), first, (|||), Arrow ((***)) )
import Control.Monad (guard, join, (<=<), void)
import Data.Either.Extra (mapLeft)
import Data.Map (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe ( fromMaybe, mapMaybe )
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Read as Text
import Data.Tuple (swap)
import Data.Void (Void)
import Day (Day (Day))
import qualified Data.Maybe as Maybe
import qualified Debug.Trace as Debug
import qualified Data.List as List
import qualified Data.List.Extra as List
import qualified Data.Char as Char
import qualified Data.PQueue.Min as PQ
import Data.Bifunctor (Bifunctor(bimap))
import Control.Monad.Extra (MonadPlus)
import qualified Data.Bits as Bits
import Text.Megaparsec hiding (Pos, match)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer hiding (space)
import Data.Functor (($>))
import Data.MemoTrie (memo)

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . (fst . findStable) . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . parseInput

type Input = Seabed

type Pos = (Int,Int)
data Seabed
  = Seabed
  { cs :: Map Pos Cucumber
  , dim :: Pos
  }
  deriving (Show, Eq, Ord)

data Cucumber
  = East
  | South
  deriving (Show, Eq, Ord)

findStable :: Seabed -> (Int, Seabed)
findStable = findStable' 1

findStable' :: Int -> Seabed -> (Int, Seabed)
findStable' n bed =
  let (moved, bed') = moveOne bed
  in if moved
      then findStable' (n+1) bed'
      else (n, bed)

moveOne :: Seabed -> (Bool, Seabed)
moveOne bed =
  let (emoved, bed') = moveEast bed
      (smoved, bed'') = moveSouth bed'
  in (emoved || smoved, bed'')

moveEast, moveSouth :: Seabed -> (Bool,Seabed)
moveEast sb@Seabed{..} = move (east dim) (west dim) East sb
moveSouth sb@Seabed{..} = move (south dim) (north dim) South sb

move :: (Pos -> Pos) -> (Pos -> Pos) -> Cucumber -> Seabed -> (Bool,Seabed)
move next prev cucumber sb@Seabed{..} =
    let moves :: [(Pos,Pos)]
        moves =
          -- Maybe.catMaybes
          -- . fmap (collectTrain . fst)
          fmap ((\p -> (p, next p)) . fst)
          . filter (Maybe.isNothing . (`Map.lookup` cs) . next . fst)
          . filter ((== cucumber) . snd)
          . Map.toList
          $ cs

        cs' = List.foldl' (.) id
                (fmap (\(from, to) m ->
                        Map.delete from
                        . Map.insert to cucumber
                        $ m) moves)
                cs

    in (not . null $ moves, sb { cs = cs' })

  where
    collectTrain :: Pos -> Maybe (Pos,Pos)
    collectTrain p = (,next p) <$> findTail p p

    findTail :: Pos -> Pos -> Maybe Pos
    findTail s p | prev p == s = Nothing
    findTail s p =
      case Map.lookup (prev p) cs of
        Just cucumber' | cucumber == cucumber' -> findTail s (prev p)
        _ -> Just p

west :: Pos -> Pos -> Pos
west _ (x,y) | x > 0 = (x-1,y)
west dim (_,y) = (fst dim - 1, y)

east :: Pos -> Pos -> Pos
east dim (x,y) | x < fst dim - 1 = (x+1,y)
east _ (_,y) = (0, y)

north :: Pos -> Pos -> Pos
north _ (x,y) | y > 0 = (x,y-1)
north dim (x,_) = (x, snd dim - 1)

south :: Pos -> Pos -> Pos
south dim (x,y) | y < snd dim - 1 = (x, y+1)
south _ (x,_) = (x, 0)

type P = Parsec Void Text

parseInput :: Text -> Seabed
parseInput input =
  let ls = Text.lines input
      dim = (Text.length $ head ls, length ls)
      cs = Map.fromList $ do
        (y,line) <- [0..] `zip` ls
        (x,ch) <- [0..] `zip` Text.unpack line
        c <- case ch of
          '>' -> [ East ]
          'v' -> [ South ]
          _ -> []
        pure ((x,y),c)

  in Seabed {..}
