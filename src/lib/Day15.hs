module Day15 (
  day,
  parseInput,
  cheapestPathRisk,
  cheapestPath,
  upsizeCave
) where

import Control.Arrow (second, (&&&))
import Control.Monad (guard, join)
import Data.Either.Extra (mapLeft)
import Data.Map (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe ( fromMaybe, mapMaybe )
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Read as Text
import Data.Tuple (swap)
import Data.Void (Void)
import Day (Day (Day))
import qualified Data.Maybe as Maybe
import qualified Debug.Trace as Debug
import Text.Megaparsec hiding (Pos)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer hiding (space)
import qualified Data.List as List
import qualified Data.List.Extra as List
import qualified Data.Char as Char
import qualified Data.PQueue.Min as PQ
import Data.Bifunctor (Bifunctor(bimap))

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . cheapestPathRisk . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . cheapestPathRisk . upsizeCave 5. parseInput

type Pos = (Int, Int)
type Cave = Map Pos Int
type Input = Cave

type Path = [(Pos, Int)]

cheapestPathRisk :: Cave -> Maybe Int
cheapestPathRisk cave = do
  path <- cheapestPath cave
  Maybe.listToMaybe $ snd <$> reverse path

astar :: (Pos -> [Pos]) -> (Pos -> Pos -> Int) -> Cave -> Pos -> Pos -> Maybe Path
astar ns h cave start end =
    astar' (PQ.singleton (0,start)) (Map.singleton start (start,0))
  where
    astar' :: PQ.MinQueue (Int,Pos) -> Map Pos (Pos, Int) -> Maybe Path
    astar' openSet cameFrom = do
      ((fcur, cur), openSet') <- PQ.minView openSet

      if cur == end
      then
        Just $ reverse $ reconstruct cameFrom (cur,fcur)
      else
        let consider :: Pos -> Maybe (Pos, Int, Int)
            consider n =
              let (Just (_,g)) = Map.lookup cur cameFrom
                  (Just risk) = Map.lookup n cave
                  tg = g + risk
              in case snd <$> Map.lookup n cameFrom of
                    Nothing -> Just (n, tg, tg + h n end)
                    Just g' | tg < g' -> Just (n, tg, tg + h n end)
                    _ -> Nothing

            newNs = Maybe.catMaybes $ consider <$> ns cur
            insertOs = (\(n,_,f) -> PQ.insert (f,n)) <$> newNs
            insertCFs = (\(n,g,_) -> Map.insert n (cur,g)) <$> newNs

            openSet'' = foldl (.) id insertOs openSet'
            cameFrom' = foldl (.) id insertCFs cameFrom

        in
           -- Debug.trace (show openSet'') $
           astar' openSet'' cameFrom'

    reconstruct :: Map Pos (Pos, Int) -> (Pos,Int) -> Path
    reconstruct cameFrom (cur,ccost) = case Map.lookup cur cameFrom of
      Just (prev, pcost) | prev /= cur -> (cur,ccost):reconstruct cameFrom (prev,pcost)
      _ -> [(cur,ccost)]


cheapestPath :: Cave -> Maybe Path
cheapestPath cave =
  let lowerright = maximum $ Map.keys cave
  in astar (neighHV lowerright) manhattan cave (0,0) lowerright

neighHV :: (Int,Int) -> Pos -> [Pos]
neighHV (mx,my) (x,y) = [ (x',y') | (dx,dy) <- [(1,0),(0,1),(-1,0),(0,-1)]
                                  , let (x',y') = (x+dx,y+dy)
                                  , x' >=0 && x' <= mx && y' >= 0 && y' <= my
                                  ]

manhattan :: Pos -> Pos -> Int
manhattan (x1,y1) (x2,y2) = abs (x2-x1) + abs (y2-y1)

upsizeCave :: Int -> Cave -> Cave
upsizeCave n cave =
  let (lx,ly) = bimap (+1) (+1) $ maximum $ Map.keys cave
  in Map.fromList
     . join
     . fmap (\((x,y),r) ->
               [ ((x',y'),r') | i <- [0..n-1]
                              , j <- [0..n-1]
                              , let x' = x + lx*i
                                    y' = y + ly*j
                                    r' = if r+i+j <= 9 then r+i+j else r+i+j-9
                              ] )
     . Map.toList
     $ cave


parseInput :: Text -> Cave
parseInput input = Map.fromList $ do
  (y,line) <- [0..] `zip `Text.lines input
  (x,c) <- [0..] `zip` (Char.digitToInt <$> Text.unpack line)
  pure ((x,y),c)
