module Day
  ( Day (..),
  )
where

import Data.Text (Text)

data Day = Day
  { challengeOne :: Text -> Text,
    challengeTwo :: Text -> Text
  }
