module Day22 (
  day,
  parseInput,
  intersect,
  csize,
  Cuboid(..),
  runSteps,
  scanSteps
) where

import Control.Arrow ( second, (&&&), first, (|||), Arrow ((***)) )
import Control.Monad (guard, join, (<=<), void)
import Data.Either.Extra (mapLeft)
import Data.Map (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe ( fromMaybe, mapMaybe )
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Read as Text
import Data.Tuple (swap)
import Data.Void (Void)
import Day (Day (Day))
import qualified Data.Maybe as Maybe
import qualified Debug.Trace as Debug
import qualified Data.List as List
import qualified Data.List.Extra as List
import qualified Data.Char as Char
import qualified Data.PQueue.Min as PQ
import Data.Bifunctor (Bifunctor(bimap))
import Control.Monad.Extra (MonadPlus)
import qualified Data.Bits as Bits
import Text.Megaparsec hiding (Pos, match)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer hiding (space)
import Data.Functor (($>))
import Data.MemoTrie (memo)

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . fmap (csize . intersect (C (-50,-50,-50) (50,50,50)) . runSteps) . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . fmap (csize . runSteps) . parseInput


type V = (Integer,Integer,Integer)

data Cuboid = C V V
  deriving (Eq,Ord,Show)

type Cuboids = Set Cuboid

intersect :: Cuboid -> Cuboids -> Cuboids
intersect c = Set.fromList . join . fmap (Maybe.maybeToList . intersect1 c) . Set.toList

intersect1 :: Cuboid -> Cuboid -> Maybe Cuboid
intersect1 (C (xmn1,ymn1,zmn1) (xmx1,ymx1,zmx1))
           (C (xmn2,ymn2,zmn2) (xmx2,ymx2,zmx2))
  = validate $ C (max xmn1 xmn2, max ymn1 ymn2, max zmn1 zmn2)
                 (min xmx1 xmx2, min ymx1 ymx2, min zmx1 zmx2)

validate :: Cuboid -> Maybe Cuboid
validate c@(C (xmn,ymn,zmn) (xmx,ymx,zmx)) = do
  guard $ xmx >= xmn && ymx >= ymn && zmx >= zmn
  Just c

csize :: Cuboids -> Integer
csize = sum . fmap csize1 . Set.toList

csize1 :: Cuboid -> Integer
csize1 (C (xmn,ymn,zmn) (xmx,ymx,zmx)) = (xmx-xmn+1)*(ymx-ymn+1)*(zmx-zmn+1)

runSteps :: [Step] -> Cuboids
runSteps = last . scanSteps

scanSteps :: [Step] -> [Cuboids]
scanSteps = List.scanl' runStep Set.empty
  where
    runStep :: Cuboids -> Step -> Cuboids
    runStep cs (Off c) =
      Set.fromList
      . join
      . fmap (splitBy c)
      . Set.toList
      $ cs
    runStep cs (On c) =
      Set.insert c (runStep cs (Off c))

    splitBy :: Cuboid -> Cuboid -> [Cuboid]
    splitBy s c =
      let (u,c2,d) = splitX s (Just c)
          (f,c3,b) = splitY s c2
          (l,_,r) = splitZ s c3
      in Maybe.catMaybes [u,d,f,b,l,r]

    splitX,splitY,splitZ :: Cuboid -> Maybe Cuboid -> (Maybe Cuboid, Maybe Cuboid, Maybe Cuboid)

    splitX _ Nothing = (Nothing,Nothing,Nothing)
    splitX (C (s_xmn,s_ymn,s_zmn) (s_xmx,s_ymx,s_zmx))
           (Just (C (c_xmn,c_ymn,c_zmn) (c_xmx,c_ymx,c_zmx))) =
      ( validate $ C (c_xmn,c_ymn,c_zmn) (min c_xmx (s_xmn-1),c_ymx,c_zmx)
      , validate $ C (max c_xmn s_xmn,c_ymn,c_zmn) (min c_xmx s_xmx,c_ymx,c_zmx)
      , validate $ C (max c_xmn (s_xmx+1),c_ymn,c_zmn) (c_xmx,c_ymx,c_zmx)
      )

    splitY _ Nothing = (Nothing,Nothing,Nothing)
    splitY (C (s_xmn,s_ymn,s_zmn) (s_xmx,s_ymx,s_zmx))
           (Just (C (c_xmn,c_ymn,c_zmn) (c_xmx,c_ymx,c_zmx))) =
      ( validate $ C (c_xmn,c_ymn,c_zmn) (c_xmx,min c_ymx (s_ymn-1),c_zmx)
      , validate $ C (c_xmn,max c_ymn s_ymn,c_zmn) (c_xmx,min c_ymx s_ymx,c_zmx)
      , validate $ C (c_xmn,max c_ymn (s_ymx+1),c_zmn) (c_xmx,c_ymx,c_zmx)
      )
    splitZ _ Nothing = (Nothing,Nothing,Nothing)
    splitZ (C (s_xmn,s_ymn,s_zmn) (s_xmx,s_ymx,s_zmx))
           (Just (C (c_xmn,c_ymn,c_zmn) (c_xmx,c_ymx,c_zmx))) =
      ( validate $ C (c_xmn,c_ymn,c_zmn) (c_xmx,c_ymx,min c_zmx (s_zmn-1))
      , validate $ C (c_xmn,c_ymn,max c_zmn s_zmn) (c_xmx,c_ymx,min c_zmx s_zmx)
      , validate $ C (c_xmn,c_ymn,max c_zmn (s_zmx+1)) (c_xmx,c_ymx,c_zmx)
      )

data Step = On Cuboid
          | Off Cuboid
  deriving (Eq,Ord,Show)

type Input = [Step]

type P = Parsec Void Text

parseInput :: Text -> Either Text Input
parseInput = mapLeft (Text.pack . show) . runParser inputP ""

inputP :: P Input
inputP = stepP `sepEndBy` newline

stepP :: P Step
stepP = do
  k <- choice [ string "on" $> On
              , string "off" $> Off
              ]
  void $ string " x="
  xmn <- signed space decimal
  void $ string ".."
  xmx <- signed space decimal
  void $ string ",y="
  ymn <- signed space decimal
  void $ string ".."
  ymx <- signed space decimal
  void $ string ",z="
  zmn <- signed space decimal
  void $ string ".."
  zmx <- signed space decimal
  pure $ k $ C (xmn,ymn,zmn) (xmx,ymx,zmx)
