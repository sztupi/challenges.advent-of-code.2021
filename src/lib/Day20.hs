module Day20 (
  day,
  parseInput,
  reorient,
  Board,
  step
) where

import Control.Arrow ( second, (&&&), first, (|||), Arrow ((***)) )
import Control.Monad (guard, join, (<=<))
import Data.Either.Extra (mapLeft)
import Data.Map (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe ( fromMaybe, mapMaybe )
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Read as Text
import Data.Tuple (swap)
import Data.Void (Void)
import Day (Day (Day))
import qualified Data.Maybe as Maybe
import qualified Debug.Trace as Debug
import qualified Data.List as List
import qualified Data.List.Extra as List
import qualified Data.Char as Char
import qualified Data.PQueue.Min as PQ
import Data.Bifunctor (Bifunctor(bimap))
import Control.Monad.Extra (MonadPlus)
import qualified Data.Bits as Bits
import Text.Megaparsec hiding (Pos, match)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer hiding (space)
import Data.Functor (($>))
import Data.MemoTrie (memo)

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . fmap (Set.size . step 2) . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . fmap (Set.size . step 50) . parseInput

type Input = (ConvMatrix,Board)

type ConvMatrix = Set Int
type Board = Set Pos
type Pos = (Int,Int)

step :: Int -> Input -> Board
step n (cnv,brd) =
    let finalPoints :: Set Pos
        finalPoints =
          List.foldl' (.) id (replicate n (Set.fromList . (surround <=< Set.toList))) brd

    in Set.fromList [ p | p <- Set.toList finalPoints
                        , mgen (n,p)
                        ]
  where
    mgen :: (Int,Pos) -> Bool
    mgen = memo (uncurry gen)

    gen :: Int -> Pos -> Bool
    gen 0 p = Set.member p brd
    gen n p = conv cnv (curry mgen (n-1)) p

surround :: Pos -> [Pos]
surround (x,y) = [ (x+dx,y+dy) | dx <- [-1..1], dy <- [-1..1] ]

conv :: ConvMatrix -> (Pos -> Bool) -> Pos -> Bool
conv cnv bp (x,y) =
  let idx = sum $ [ 2^(i*3+j) | (i,dy) <- [2,1,0] `zip` [-1..1]
                              , (j,dx) <- [2,1,0] `zip` [-1..1]
                              , bp (x+dx, y+dy)
                              ]
  in Set.member idx cnv


reorient :: Board -> Board
reorient b =
  let mn = minimum b
  in Set.map (`pminus` mn) b

padd :: Pos -> Pos -> Pos
padd (x1,y1) (x2,y2) = (x1+x2,y1+y2)

pminus :: Pos -> Pos -> Pos
pminus a b = a `padd` pneg b

pneg :: Pos -> Pos
pneg (x1,y1) = (-x1,-y1)

type P = Parsec Void Text

parseInput :: Text -> Either Text Input
parseInput = mapLeft (Text.pack . show) . runParser inputP ""

inputP :: P Input
inputP = ((,) <$> convP <*> (newline *> boardP)) <* eof

convP :: P ConvMatrix
convP = do
  bits <- some (choice
                [ char '.' $> False
                , char '#' $> True
                ]) <* newline
  pure $ Set.fromList $ fmap fst $ filter snd $ [0..] `zip` bits


boardP :: P Board
boardP =
  do
    lines <- ([0..] `zip`) <$> lineP `sepEndBy` newline
    pure
      $ Set.fromList
      $ [ (x,y) | (y,line) <- lines
                , (x,bit) <- line
                , bit ]

  where
    lineP :: P [(Int,Bool)]
    lineP = ([0..] `zip`)
            <$> some (choice
                [ char '.' $> False
                , char '#' $> True
                ])
