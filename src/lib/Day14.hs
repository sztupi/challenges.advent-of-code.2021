module Day14 (
  day,
  parseInput,
  Input(..),
  pairs,
  diffAfter,
  diffAfterPairs,
  insertion,
  insertionPairs,
) where

import Control.Arrow (second, (&&&))
import Control.Monad (guard, join)
import Data.Either.Extra (mapLeft)
import Data.Map (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe ( fromMaybe, mapMaybe )
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Read as Text
import Data.Tuple (swap)
import Data.Void (Void)
import Day (Day (Day))
import qualified Debug.Trace as Debug
import Text.Megaparsec hiding (Pos)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer hiding (space)
import qualified Data.List as List
import qualified Data.List.Extra as List

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . fmap (diffAfter 10). parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . fmap (diffAfterPairs 40). parseInput

type Rules = Map (Char,Char) Char
data Input = Input String Rules
  deriving (Show, Eq)

diffAfter :: Int -> Input -> Int
diffAfter n (Input start rules) =
  let (mn,mx) = minmaxCount $ run n rules start
  in mx-mn

diffAfterPairs :: Int -> Input -> Int
diffAfterPairs n (Input start rules) =
  let end = Map.toList $ runPairs n rules (pairs start)
      charCounts = Map.toList $ Map.fromListWith (+) $ (head start,1):((\((a,b),n) -> (b,n)) <$> end)
  in List.maximum (snd <$> charCounts) - List.minimum (snd <$> charCounts)

minmaxCount :: (Eq a, Ord a) => [a] -> (Int,Int)
minmaxCount as =
  let counts = Map.toList . Map.fromListWith (+) $ (,1) <$> as
  in  (List.minimum $ snd <$> counts, List.maximum $ snd <$> counts)

run :: Int -> Rules -> String -> String
run n rules = List.foldl' (.) id (replicate n (insertion rules))

runPairs :: Int -> Rules -> Map (Char,Char) Int -> Map (Char,Char) Int
runPairs n rules = List.foldl' (.) id (replicate n (insertionPairs rules))

pairs :: String -> Map (Char,Char) Int
pairs s = Map.fromListWith (+) $ (,1) <$> (s `zip` drop 1 s)

insertionPairs :: Rules -> Map (Char,Char) Int -> Map (Char,Char) Int
insertionPairs rules = Map.fromListWith (+) . join . fmap ip . Map.toList
  where
    ip :: ((Char,Char),Int) -> [((Char,Char),Int)]
    ip ((a,b),n) = case Map.lookup (a,b) rules of
      Just c -> [((a,c),n),((c,b),n)]
      Nothing -> [((a,b),n)]

insertion :: Rules -> String -> String
insertion rules s =
    case s of
      "" -> ""
      (a:rs) -> a:insertion' (a:rs)
  where
    insertion' :: String -> String
    insertion' = \case
      "" -> ""
      [b] -> []
      (a:b:rs) -> case Map.lookup (a,b) rules of
        Just c -> c:b:insertion' (b:rs)
        Nothing -> b:insertion' (b:rs)

type P = Parsec Void Text

parseInput :: Text -> Either Text Input
parseInput = mapLeft (Text.pack . show) . runParser inputP ""

inputP :: P Input
inputP = Input <$> templateP
               <*> (newline *> rulesP)

templateP :: P String
templateP = someTill upperChar newline

rulesP :: P Rules
rulesP = Map.fromList <$> ruleP `sepEndBy` newline

ruleP :: P ((Char,Char),Char)
ruleP = (,) <$> ((,) <$> upperChar <*> upperChar)
            <*> (string " -> " *> upperChar)
