module Day17 (
  day,
  parseInput,
  highest,
  findAllHits
) where

import Control.Arrow ( second, (&&&), first )
import Control.Monad (guard, join)
import Data.Either.Extra (mapLeft)
import Data.Map (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe ( fromMaybe, mapMaybe )
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Read as Text
import Data.Tuple (swap)
import Data.Void (Void)
import Day (Day (Day))
import qualified Data.Maybe as Maybe
import qualified Debug.Trace as Debug
import qualified Data.List as List
import qualified Data.List.Extra as List
import qualified Data.Char as Char
import qualified Data.PQueue.Min as PQ
import Data.Bifunctor (Bifunctor(bimap))
import Control.Applicative (Alternative(..))
import Control.Monad.Extra (MonadPlus)
import qualified Data.Bits as Bits
import Text.Megaparsec hiding (Pos)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer hiding (space)

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . fmap highest . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . fmap (length . findAllHits) . parseInput

type Input = Target
type Target = ((Int,Int),(Int,Int))

highest :: Input -> Int
highest (_,(mny,_)) = sum [0..(-mny)-1]

testHit :: Target -> (Int,Int) -> (Int,Int) -> Bool
testHit target@((mnx,mxx),(mny,mxy)) (vx,vy) (px,py)
  | px > mxx || py < mny = False
  | px <= mxx && px >= mnx && py <= mxy && py >= mny = True
  | otherwise = testHit target
        (if vx > 0 then vx-1 else vx, vy-1)
        (px+vx,py+vy)

findAllHits :: Target -> [(Int,Int)]
findAllHits target@((mnx,mxx),(mny,mxy)) =
  [ (x,y) | x <- [0..mxx]
          , y <- [mny..(-mny)]
          , testHit target (x,y) (0,0)
          ]

type P = Parsec Void Text

parseInput :: Text -> Either Text Input
parseInput = mapLeft (Text.pack . show) . runParser inputP ""

inputP :: P Input
inputP = (,) <$> ((,) <$> (string "target area: x=" *> decimal) <*> (string ".." *> decimal))
             <*> ((,) <$> (string ", y=" *> signed space decimal) <*> (string ".." *> signed space decimal))
