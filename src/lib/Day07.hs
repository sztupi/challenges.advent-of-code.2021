module Day07 (
  day,
  parseInput,
  leastDistanceSum,
  leastDistanceIncSum
) where

import Control.Arrow ((&&&), second)
import Control.Monad
import Data.Char (digitToInt)
import Data.Either
import Data.Either.Extra
import Data.Function (on)
import Data.List (sortBy)
import qualified Data.List as List
import Data.Map (Map)
import Data.Set (Set)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.Maybe (fromMaybe, listToMaybe)
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Read as Text
import Data.Void (Void)
import Day (Day (Day))
import Text.Megaparsec hiding (Pos)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer hiding (space)
import Data.Vector (Vector, (!))
import qualified Data.Vector as Vec

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . snd . leastDistanceSum . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . sum . leastDistanceIncSum . parseInput

parseInput :: Text -> [Int]
parseInput = fmap fst . rights . fmap Text.decimal . Text.splitOn ","

leastDistanceFSum :: (Int -> Int) -> [Int] -> (Int,Int)
leastDistanceFSum f is = head . List.sortOn snd . fmap (id &&& distanceSum is) $ [minimum is .. maximum is]
  where
    distanceSum is i = sum $ fmap (f . abs . (-) i) is

leastDistanceSum = leastDistanceFSum id

leastDistanceIncSum = leastDistanceFSum (\n -> (n*(n+1)) `div` 2)
