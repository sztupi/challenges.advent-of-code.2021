module Day01 (
  day,
  countIncreases,
  windowSumIncreases,
) where

import Data.Either
import Data.List
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Read as Text
import Day (Day (Day))

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . countIncreases . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . windowSumIncreases 3 . parseInput

parseInput :: Text -> [Int]
parseInput = fmap fst . rights . fmap Text.decimal . Text.lines

countIncreases :: (Num a, Ord a) => [a] -> Int
countIncreases [] = 0
countIncreases as = length $ filter (< 0) $ zipWith (-) as (drop 1 as)

windows :: Int -> [a] -> [[a]]
windows n = filter ((== n) . length) . map (take n) . tails

windowSumIncreases :: (Num a, Ord a) => Int -> [a] -> Int
windowSumIncreases n = countIncreases . fmap sum . windows n
