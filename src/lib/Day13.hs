module Day13 (
  day,
  parseInput,
  Pos,
  Sheet,
  Input(..),
  Folding(..),
  applyfold
) where

import Control.Arrow (second, (&&&))
import Control.Monad (guard, join)
import Data.Either.Extra (mapLeft)
import Data.Map (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe ( fromMaybe, mapMaybe )
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Read as Text
import Data.Tuple (swap)
import Data.Void (Void)
import Day (Day (Day))
import qualified Debug.Trace as Debug
import Text.Megaparsec hiding (Pos)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer hiding (space)
import qualified Data.List as List

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . fmap (\i -> Set.size $ applyfold (head $ foldings i) (sheet i)) . parseInput

challengeTwo :: Text -> Text
challengeTwo = either id (prettyPrint . process) . parseInput

type Pos = (Int, Int)

type Sheet = Set Pos

data Folding = FoldX Int | FoldY Int
  deriving (Eq, Ord, Show)

data Input = Input
  { sheet :: Sheet
  , foldings :: [Folding]
  }
  deriving (Eq, Ord, Show)

applyfold :: Folding -> Sheet -> Sheet
applyfold f = Set.fromList . mapMaybe (applyfold' f) . Set.toList
  where
    applyfold' :: Folding -> Pos -> Maybe Pos
    applyfold' (FoldX ax) (x,y) | x < ax = Just (x,y)
    applyfold' (FoldX ax) (x,y) | x == ax = Nothing
    applyfold' (FoldX ax) (x,y) = Just (2*ax-x,y)
    applyfold' (FoldY ay) (x,y) | y < ay = Just (x,y)
    applyfold' (FoldY ay) (x,y) | y == ay = Nothing
    applyfold' (FoldY ay) (x,y) = Just (x,2*ay-y)

process :: Input -> Sheet
process (Input sheet fs) = List.foldl' (flip applyfold) sheet fs

prettyPrint :: Sheet -> Text
prettyPrint s =
  let ss = Set.toList s
      mx = maximum (fst <$> ss)
      my = maximum (snd <$> ss)
  in Text.pack $
     join
     [ [ c
       | x <- [0..mx]
       , let c = if (x,y) `Set.member` s then '#' else '.'
       ] ++ "\n"
     | y <- [0..my]
     ]


type P = Parsec Void Text

parseInput :: Text -> Either Text Input
parseInput = mapLeft (Text.pack . show) . runParser inputP ""

inputP :: P Input
inputP = Input
         <$> (Set.fromList <$> pointsP)
         <*> (newline *> foldingsP )

pointsP :: P [Pos]
pointsP = posP `sepEndBy` newline

posP :: P Pos
posP = (,) <$> decimal <*> (char ',' *> decimal)

foldingsP :: P [Folding]
foldingsP = foldingP `sepEndBy` newline

foldingP ::P Folding
foldingP =
  string "fold along " *>
  choice
  [ FoldX <$> (string "x=" *> decimal)
  , FoldY <$> (string "y=" *> decimal)
  ]
