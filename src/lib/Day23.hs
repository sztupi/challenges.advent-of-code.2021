module Day23 (
  day,
  parseInput,
  solve1,
  moves,
  vis,
  injectMissing
) where

import Control.Arrow (Arrow ((***)), first, second, (&&&), (|||))
import Control.Monad (guard, join, void, (<=<))
import Control.Monad.Extra (MonadPlus)
import Data.Bifunctor (Bifunctor (bimap))
import qualified Data.Bits as Bits
import qualified Data.Char as Char
import Data.Either.Extra (mapLeft)
import Data.Functor (($>))
import qualified Data.List as List
import qualified Data.List.Extra as List
import Data.Map (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe (fromMaybe, mapMaybe, maybeToList)
import qualified Data.Maybe as Maybe
import Data.MemoTrie (memo)
import qualified Data.PQueue.Min as PQ
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Read as Text
import Data.Tuple (swap)
import Data.Void (Void)
import Day (Day (Day))
import qualified Debug.Trace as Debug
import Text.Megaparsec hiding (Pos, match)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer hiding (space)
import Data.Tuple.Extra (snd3)

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . fmap solve1 . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . fmap solve1 . parseInput . injectMissing

data Apod = A | B | C | D
  deriving (Eq, Ord, Show)

data NType
  = Home Apod
  | Nonstop
  | Stop
  deriving (Eq, Ord, Show)

data Node = Node {ntype :: NType, npod :: Maybe Apod}
  deriving (Eq, Ord, Show)

type Pos = (Int, Int)
type St = Map Pos Node

type Input = St

solve1 :: Input -> Maybe Int
solve1 startSt = step (PQ.singleton (0, startSt)) Set.empty
 where
  step :: PQ.MinQueue (Int, St) -> Set St -> Maybe Int
  step (PQ.minView -> Nothing) _ = Nothing
  step (PQ.minView -> Just ((cost, st), _)) _ | isFinal st = Just cost
  step (PQ.minView -> Just ((cost, st), pq)) visited | st `Set.member` visited = step pq visited
  step (PQ.minView -> Just ((cost, st), pq)) visited =
    let sts' = map (first (+ cost)) . filter ((`Set.notMember` visited) . snd) $ moves st
        pq' = List.foldl' (.) id [PQ.insert st' | st' <- sts'] $ pq
     in step pq' (st `Set.insert` visited)

maxY :: St -> Int
maxY = maximum . fmap snd . Map.keys

moves :: St -> [(Int, St)]
moves st = do
  (pos, pod) <- pods st
  movesFrom pos [(0,pos,[st])] []

movesFrom :: Pos -> [(Int, Pos, [St])] -> [(Int, St)] -> [(Int, St)]
movesFrom _ [] ends = ends
movesFrom start ((cost,pos,sts@(st:_)):stss) ends =
  let sts' = do
        pod <- maybeToList $ Map.lookup pos st >>= npod
        pos' <- freeNeighbours st pos
        let st' =
              Map.update (\(Node nt _) -> Just $ Node nt Nothing) pos
                . Map.update (\(Node nt _) -> Just $ Node nt (Just pod)) pos'
                $ st
            cost' = cost + stepCost pod
        guard $ st' `notElem` sts

        pure (cost',pos',st':sts)

      ends' = filter (\(_,p,s:_) -> isValidMove (maxY s) start p s) sts'
    in movesFrom start (sts'<>stss) (((\(c,_,s:_) -> (c,s)) <$> ends') <> ends)

isValidMove :: Int -> Pos -> Pos -> St -> Bool
isValidMove maxY start@(sx,sy) end@(ex,ey) st =
  (sy == 0
    && ey /= 0
    && ( all (\y -> Maybe.isNothing $ npod =<< Map.lookup (ex,y) st) [1..(ey-1)]
         &&
         all (\y -> maybe False
               (\case (Node (Home h) (Just a)) -> h == a; _ -> False)
               (Map.lookup (ex,y) st))
         [ey..maxY]
       )
  )
  ||
  (sy /= 0
   && ey == 0
   && case Map.lookup (ex,ey) st of
        Just (Node Stop _) -> True
        _ -> False
  )

stepCost :: Apod -> Int
stepCost A = 1
stepCost B = 10
stepCost C = 100
stepCost D = 1000

vis :: St -> String
vis st =
  join $
    [ l <> "\n"
    | y <- [0 .. 2]
    , let l = [ c
              | x <- [0 .. 10]
              , let c = case Map.lookup (x, y) st of
                          Nothing -> ' '
                          Just (Node _ (Just p)) -> visp p
                          _ -> '.'
              ]
    ]
 where
  visp A = 'A'
  visp B = 'B'
  visp C = 'C'
  visp D = 'D'

pods :: St -> [(Pos, Apod)]
pods = mapMaybe (\(p, n) -> (p,) <$> npod n) . Map.toList

freeNeighbours :: St -> Pos -> [Pos]
freeNeighbours st (x, y) = do
  (dx, dy) <- [(-1, 0), (0, -1), (1, 0), (0, 1)]
  let (x', y') = (x + dx, y + dy)
  case Map.lookup (x', y') st of
    Nothing -> []
    Just n -> case npod n of
      Just _ -> []
      Nothing -> pure (x', y')

isValidStep :: Pos -> St -> Bool
isValidStep p@(x,y) st =
  (maybe False isNodeValidStep $ Map.lookup p st)
  &&
  (if y /= 1
    then True
    else case Map.lookup (x,2) st of
           Just (Node (Home a) (Just b)) -> a == b
           _ -> True)
 where
  isNodeValidStep :: Node -> Bool
  isNodeValidStep (Node _ Nothing) = True
  isNodeValidStep (Node Stop (Just _)) = True
  isNodeValidStep (Node (Home a) (Just b)) = a == b
  isNodeValidStep (Node Nonstop (Just _)) = False

isFinal :: St -> Bool
isFinal = all (isNodeFinal . snd) . Map.toList
 where
  isNodeFinal :: Node -> Bool
  isNodeFinal (Node (Home _) Nothing) = False
  isNodeFinal (Node (Home a) (Just b)) = a == b
  isNodeFinal (Node _ Nothing) = True
  isNodeFinal (Node _ (Just _)) = False

injectMissing :: Text -> Text
injectMissing =
  Text.unlines
  . (\(h,t) -> h <> ["  #D#C#B#A#", "  #D#B#A#C#"] <> t)
  . (take 3 &&& drop 3)
  . Text.lines

type P = Parsec Void Text

parseInput :: Text -> Either Text Input
parseInput = mapLeft (Text.pack . show) . runParser inputP ""

inputP :: P Input
inputP = do
  _ <- string "#############" >> newline
  _ <- string "#...........#" >> newline
  (a1, b1, c1, d1) <-
    (,,,) <$> (string "###" *> apodP)
      <*> (char '#' *> apodP)
      <*> (char '#' *> apodP)
      <*> (char '#' *> apodP <* string "###" <* newline)
  lines <- zip [2..] <$> do
    let line = do
          _ <- try $ lookAhead (string "  #" >> apodP)
          (,,,) <$> (string "  #" *> apodP)
            <*> (char '#' *> apodP)
            <*> (char '#' *> apodP)
            <*> (char '#' *> apodP <* string "#")
    line `sepEndBy` newline
  _ <- string "  #########" >> many newline
  pure $
    Map.fromList $
      [ ((0, 0), Node Stop Nothing)
      , ((1, 0), Node Stop Nothing)
      , ((2, 0), Node Nonstop Nothing)
      , ((3, 0), Node Stop Nothing)
      , ((4, 0), Node Nonstop Nothing)
      , ((5, 0), Node Stop Nothing)
      , ((6, 0), Node Nonstop Nothing)
      , ((7, 0), Node Stop Nothing)
      , ((8, 0), Node Nonstop Nothing)
      , ((9, 0), Node Stop Nothing)
      , ((10, 0), Node Stop Nothing)
      , ((2, 1), Node (Home A) (Just a1))
      , ((4, 1), Node (Home B) (Just b1))
      , ((6, 1), Node (Home C) (Just c1))
      , ((8, 1), Node (Home D) (Just d1))
      ] <>
      join
      [ [ ((2, y), Node (Home A) (Just a))
        , ((4, y), Node (Home B) (Just b))
        , ((6, y), Node (Home C) (Just c))
        , ((8, y), Node (Home D) (Just d))
        ]
      | (y,(a,b,c,d)) <- lines
      ]

apodP :: P Apod
apodP =
  choice
    [ char 'A' $> A
    , char 'B' $> B
    , char 'C' $> C
    , char 'D' $> D
    ]
