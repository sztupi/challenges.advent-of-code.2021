module Day09 (
  day,
  parseInput,
  lowPoints,
  basins,
) where

import Control.Arrow ((&&&), second)
import Control.Monad
import Data.Char (digitToInt, ord)
import Data.Either
import Data.Either.Extra
import Data.Function (on)
import Data.List (sortBy)
import qualified Data.List as List
import Data.Map (Map)
import Data.Set (Set)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.Maybe (fromMaybe, listToMaybe, maybeToList)
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Read as Text
import Data.Void (Void)
import Day (Day (Day))
import Text.Megaparsec hiding (Pos)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer hiding (space)
import Data.Vector (Vector, (!))
import qualified Data.Vector as Vec
import qualified Data.Tuple as Tuple

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . sum . fmap ((+ 1) . snd) . lowPoints . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . product . take 3 . reverse . List.sort . fmap Set.size . basins . parseInput

type Pos = (Int,Int)
type Height = Int
type HeightMap = Map Pos Height

parseInput :: Text -> HeightMap
parseInput input = Map.fromList $ do
  (y,line) <- zip [0..] $ Text.lines input
  (x,h) <- zip [0..] $ digitToInt <$> Text.unpack line
  pure ((x,y),h)

neighbours :: HeightMap -> Pos -> [(Pos,Height)]
neighbours m (x,y) = do
  (dx,dy) <- [(-1,0),(1,0),(0,-1),(0,1)]
  let c = (x+dx,y+dy)
  h <- maybeToList $ Map.lookup c m
  pure (c,h)

lowPoints :: HeightMap -> [(Pos, Height)]
lowPoints m = filter (\(p,h) -> all ((> h) . snd) (neighbours m p)) $ Map.toList m

basins :: HeightMap -> [Set (Pos, Height)]
basins m = basin m <$> lowPoints m

basin :: HeightMap -> (Pos, Height) -> Set (Pos, Height)
basin m p = basin' (Set.singleton p)
  where
    basin' :: Set (Pos, Height) -> Set (Pos, Height)
    basin' s =
      let allHNs = Set.fromList $ higherNeighbours =<< Set.toList s
          newHNs = allHNs `Set.difference` s
      in if Set.null newHNs then s else basin' (s `Set.union` newHNs)

    higherNeighbours :: (Pos, Height) -> [(Pos, Height)]
    higherNeighbours (p,h) = filter ((\h' -> h < h' && h' < 9) . snd) $ neighbours m p
