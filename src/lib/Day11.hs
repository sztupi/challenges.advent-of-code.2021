module Day11 (
  day,
  parseInput,
  step,
  findSync
) where

import Control.Arrow ((&&&), first, second)
import Control.Monad
import Data.Char (digitToInt, ord)
import Data.Either
import Data.Either.Extra
import Data.Function (on)
import Data.List (sortBy)
import qualified Data.List as List
import Data.Map (Map)
import Data.Set (Set)
import qualified Data.Map.Strict as Map
import qualified Data.Map.Merge.Strict as Map.Merge
import qualified Data.Set as Set
import Data.Maybe (fromMaybe, listToMaybe, maybeToList)
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Read as Text
import Data.Void (Void)
import Day (Day (Day))
import Text.Megaparsec hiding (Pos)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer hiding (space)
import Data.Vector (Vector, (!))
import qualified Data.Vector as Vec
import qualified Data.Tuple as Tuple

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . snd . step 100 . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . findSync . parseInput

type Pos = (Int,Int)
data Octopus = Level Int | Flash
  deriving (Eq,Ord,Show)
type Cavern = Map Pos Octopus

parseInput :: Text -> Cavern
parseInput input = Map.fromList $ do
  (y,line) <- zip [0..] $ Text.lines input
  (x,h) <- zip [0..] $ digitToInt <$> Text.unpack line
  pure ((x,y), Level h)

step :: Int -> Cavern -> (Cavern, Int)
step n = step' n 0
  where
    step' :: Int -> Int -> Cavern -> (Cavern, Int)
    step' 0 s c = (c,s)
    step' n s c = let (c',s') = step1 c in step' (n-1) (s+s') c'

findSync :: Cavern -> Int
findSync c =
  let (c', f) = step1 c
  in if f == Map.size c
     then 1
     else 1 + findSync c'

neighbours :: Pos -> [Pos]
neighbours (x,y) = [(x+dx,y+dy) | dx <- [-1..1], dy <- [-1..1], (dx,dy) /= (0,0) ]

step1 :: Cavern -> (Cavern, Int)
step1 = first finalize . uncurry (step1' 0) . initialStep
  where
    step1' :: Int -> [Pos] -> Cavern -> (Cavern, Int)
    step1' n [] cavern = (cavern, n)
    step1' n flashes cavern =
      let grows = Map.fromListWith (+) $ [ (np, 1) | p <- flashes, np <- neighbours p ]
          (flashes', cavern') =
            Map.Merge.mergeA
              Map.Merge.dropMissing
              Map.Merge.preserveMissing
              (Map.Merge.zipWithAMatched
                (\pos growth oct ->
                   let (oct', flash) = grow growth oct in ([pos | flash], oct')))
              grows
              cavern

      in step1' (n+length flashes) flashes' cavern'

    grow :: Int -> Octopus -> (Octopus, Bool)
    grow _ Flash = (Flash, False)
    grow i (Level n) | i+n > 9 = (Flash, True)
    grow i (Level n) = (Level $ n+i, False)

    initialStep :: Cavern -> ([Pos], Cavern)
    initialStep =
      Map.mapAccumWithKey
        (\acc pos oct -> let (oct', flash) = grow 1 oct in (if flash then pos:acc else acc, oct'))
        []

    finalize :: Cavern -> Cavern
    finalize = Map.map (\case Flash -> Level 0; oct -> oct)
