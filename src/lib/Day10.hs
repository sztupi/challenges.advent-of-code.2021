module Day10 (
  day,
  parseInput,
  Line(..),
  scoreIncomplete
) where

import Control.Arrow ((&&&), second)
import Control.Monad
import Data.Char (digitToInt, ord)
import Data.Either
import Data.Either.Extra
import Data.Function (on)
import Data.List (sortBy)
import qualified Data.List as List
import Data.Map (Map)
import Data.Set (Set)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.Maybe (fromMaybe, listToMaybe, maybeToList)
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Read as Text
import Data.Void (Void)
import Day (Day (Day))
import Text.Megaparsec hiding (Pos)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer hiding (space)
import Data.Vector (Vector, (!))
import qualified Data.Vector as Vec
import qualified Data.Tuple as Tuple

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . sum . fmap scoreCorrupt . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . middle . List.sort . filter (/= 0) . fmap scoreIncomplete . parseInput

data Line
  = Complete [Chunk]
  | Incomplete ParseState
  | Corrupt ParseState Char
  deriving (Show, Eq)

data Chunk = Chunk Char [Chunk]
  deriving (Show, Eq)

data ParseState
  = Empty [Chunk]
  | Open Char [Chunk] ParseState
  deriving (Show, Eq)

scoreCorrupt :: Line -> Int
scoreCorrupt (Corrupt _ ')')= 3
scoreCorrupt (Corrupt _ ']')= 57
scoreCorrupt (Corrupt _ '}')= 1197
scoreCorrupt (Corrupt _ '>')= 25137
scoreCorrupt _ = 0

scoreIncomplete :: Line -> Int
scoreIncomplete (Incomplete ps) = scoreParseState 0 ps
  where
    scoreParseState :: Int -> ParseState -> Int
    scoreParseState score (Empty _) = score
    scoreParseState score (Open c _ p) = scoreParseState (score * 5 + scoreChar c) p

    scoreChar :: Char -> Int
    scoreChar '(' = 1
    scoreChar '[' = 2
    scoreChar '{' = 3
    scoreChar '<' = 4
    scoreChar _ = 0

scoreIncomplete _ = 0

middle :: [a] -> a
middle l = l !! (length l `div` 2)

parseLine :: Text -> Line
parseLine = (`parseLine'` Empty []) . Text.unpack
  where
    parseLine' :: String -> ParseState -> Line
    parseLine' [] (Empty chunks) = Complete (reverse chunks)
    parseLine' [] st = Incomplete st
    parseLine' (c : cs) st | isOpen c = parseLine' cs (Open c [] st)
    parseLine' (c : cs) (Empty _)= error "unexpected input"
    parseLine' (c : cs) st@(Open o chunks parent) | not $ isPair (o,c) = Corrupt st c
    parseLine' (c : cs) st@(Open o chunks (Empty chunks')) =
      parseLine' cs (Empty (Chunk o (reverse chunks):chunks'))
    parseLine' (c : cs) st@(Open o chunks (Open o' chunks' parent)) =
      parseLine' cs (Open o' (Chunk o (reverse chunks):chunks') parent)

pairs :: [(Char, Char)]
pairs = [ ('(',')')
        , ('[',']')
        , ('{','}')
        , ('<','>')
        ]

isPair :: (Char,Char) -> Bool
isPair = (`elem` pairs)

isOpen :: Char -> Bool
isOpen = (`elem` (fst <$> pairs))

parseInput :: Text -> [Line]
parseInput = fmap parseLine . Text.lines
