module Day05 (
  day,
  ventMapHL,
  ventMapHLD,
  countOverlap,
  parseInput,
) where

import Control.Arrow ((&&&), second)
import Control.Monad
import Data.Char (digitToInt)
import Data.Either
import Data.Either.Extra
import Data.Function (on)
import Data.List (sortBy)
import qualified Data.List as List
import Data.Map (Map)
import Data.Set (Set)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.Maybe (fromMaybe, listToMaybe)
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Read as Text
import Data.Void (Void)
import Day (Day (Day))
import Text.Megaparsec hiding (Pos)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer hiding (space)

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . fmap (countOverlap . ventMapHL) . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . fmap (countOverlap . ventMapHLD) . parseInput

type Input = [Line]

type Line = (Pos,Pos)

type Pos = (Int,Int)


type VentMap = Map Pos Int

ventMapHL :: Input -> VentMap
ventMapHL lines =
  let points = fmap (,1) . join $ do
        (s, e) <- lines
        let (start,end) = if s < e then (s,e) else (e,s)
        pure $ case (start,end) of
          ((sx,sy),(ex,ey)) | sx == ex -> [ (sx,y) | y <- [sy..ey] ]
          ((sx,sy),(ex,ey)) | sy == ey -> [ (x,sy) | x <- [sx..ex] ]
          _ -> []
  in Map.fromListWith (+) points

ventMapHLD :: Input -> VentMap
ventMapHLD lines =
  let points = fmap (,1) . join $ do
        (s, e) <- lines
        let (start,end) = if s < e then (s,e) else (e,s)
        pure $ case (start,end) of
          ((sx,sy),(ex,ey)) | sx == ex -> [ (sx,y) | y <- [sy..ey] ]
          ((sx,sy),(ex,ey)) | sy == ey -> [ (x,sy) | x <- [sx..ex] ]
          ((sx,sy),(ex,ey)) | sy < ey -> [ (sx+i,sy+i) | i <- [0..(ex-sx)] ]
          ((sx,sy),(ex,ey)) -> [ (sx+i,sy-i) | i <- [0..(ex-sx)] ]
  in Map.fromListWith (+) points

countOverlap :: VentMap -> Int
countOverlap = Map.size . Map.filter (>= 2)

type P = Parsec Void Text

parseInput :: Text -> Either Text Input
parseInput = mapLeft (Text.pack . show) . runParser inputP "<none>"

inputP :: P Input
inputP = lineP `sepEndBy` newline

lineP :: P Line
lineP = (,) <$> posP <*> (string " -> " *> posP)

posP :: P Pos
posP = (,) <$> decimal <*> (char ',' *> decimal)
