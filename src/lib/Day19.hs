module Day19 (
  day,
  parseInput,
  Layout(..),
  findFullLayout,
  largestDistance
) where

import Control.Arrow ( second, (&&&), first, (|||), Arrow ((***)) )
import Control.Monad (guard, join)
import Data.Either.Extra (mapLeft)
import Data.Map (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe ( fromMaybe, mapMaybe )
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Read as Text
import Data.Tuple (swap)
import Data.Void (Void)
import Day (Day (Day))
import qualified Data.Maybe as Maybe
import qualified Debug.Trace as Debug
import qualified Data.List as List
import qualified Data.List.Extra as List
import qualified Data.Char as Char
import qualified Data.PQueue.Min as PQ
import Data.Bifunctor (Bifunctor(bimap))
import Control.Applicative (Alternative(..))
import Control.Monad.Extra (MonadPlus)
import qualified Data.Bits as Bits
import Text.Megaparsec hiding (Pos, match)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer hiding (space)

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . fmap (Set.size . lbeacons . findFullLayout) . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . fmap (largestDistance . lscanners . findFullLayout) . parseInput

type Input = [Scanner]

type Pos = (Int,Int,Int)

data Scanner
  = Scanner
  { sid :: Int
  , sbeacons :: Set Pos
  , spos :: Pos
  }
  deriving (Show, Eq, Ord)

data Layout
  = Layout
  { lbeacons :: Set Pos
  , lscanners :: Set Pos
  }
  deriving (Show, Eq, Ord)

findFullLayout :: Input -> Layout
findFullLayout [] = Layout Set.empty Set.empty
findFullLayout (s:ss) =
    let scanners = step (Set.singleton s) [s] ss
    in Layout
       { lbeacons = Set.unions $ sbeacons <$> Set.toList scanners
       , lscanners = Set.map spos scanners
       }
  where
    step :: Set Scanner -> [Scanner] -> [Scanner] -> Set Scanner
    step found _ [] = found
    step found [] nf@(_:_) = error $ show (found, nf) <> "\ncouldn't match all scanners"
    step found (m:ms) scs =
      let (ms', scs') = matches ([],[]) m scs
      in  step (List.foldl' (flip Set.insert) found ms') (ms <> ms') scs'

    matches :: ([Scanner],[Scanner]) -> Scanner -> [Scanner] -> ([Scanner], [Scanner])
    matches agg _ [] = agg
    matches (as,bs) m (s:ss) =
      case match m s of
        Just s' -> matches (s':as,bs) m ss
        Nothing -> matches (as,s:bs) m ss

match :: Scanner -> Scanner -> Maybe Scanner
match a b = Maybe.listToMaybe $ do
  b' <- variations b
  case matchesBeacons (sbeacons a) (sbeacons b') of
    Just (shifted,delta) -> [b' { sbeacons = shifted, spos = delta } ]
    Nothing -> []

variations :: Scanner -> [Scanner]
variations s = do
  rotate <- [ \(x,y,z) -> (x,z,-y)
            , \(x,y,z) -> (x,-y,-z)
            , \(x,y,z) -> (x,-z,y)
            , id
            ]
  orient <- [ \(x,y,z) -> (-z,y,x)
            , \(x,y,z) -> (-x,y,-z)
            , \(x,y,z) -> (z,y,-x)
            , \(x,y,z) -> (-y,x,z)
            , \(x,y,z) -> (y,-x,z)
            , id
            ]
  pure $ s { sbeacons = Set.map (orient . rotate) $ sbeacons s }

matchesBeacons :: Set Pos -> Set Pos -> Maybe (Set Pos, Pos)
matchesBeacons refs bs = Maybe.listToMaybe $ do
  r <- Set.toList refs
  b <- Set.toList bs
  let d = r `pminus` b
      bs' = Set.map (padd d) bs
  [ (bs',d) | Set.size (refs `Set.intersection` bs') >= 12 ]

padd :: Pos -> Pos -> Pos
padd (x1,y1,z1) (x2,y2,z2) = (x1+x2,y1+y2,z1+z2)

pminus :: Pos -> Pos -> Pos
pminus a b = a `padd` pneg b

pneg :: Pos -> Pos
pneg (x1,y1,z1) = (-x1,-y1,-z1)

manhattan :: Pos -> Pos -> Int
manhattan a b = (\(dx,dy,dz) -> sum $ abs <$> [dx,dy,dz] ) $ a `pminus` b

largestDistance :: Set Pos -> Int
largestDistance (Set.toList->s) = maximum [ manhattan a b | a <- s, b <- s ]

shift :: Scanner -> Pos -> Scanner
shift s delta = s { sbeacons = Set.map (padd delta) $ sbeacons s }

type P = Parsec Void Text

parseInput :: Text -> Either Text Input
parseInput = mapLeft (Text.pack . show) . runParser inputP ""

inputP :: P Input
inputP = scannerP `sepEndBy` newline

scannerP :: P Scanner
scannerP = do
  sid <- string "--- scanner " *> decimal <* string " ---"
  _ <- newline
  sbeacons <- Set.fromList <$> pointP `sepEndBy` newline
  pure $ Scanner{spos = (0,0,0), ..}

pointP :: P Pos
pointP = (,,) <$> signed space decimal
              <*> (char ',' *> signed space decimal)
              <*> (char ',' *> signed space decimal)
