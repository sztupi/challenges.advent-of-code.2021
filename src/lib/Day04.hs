module Day04 (
  day,
  Input(..),
  Board,
  mkBoard,
  findBingo,
  parseInput,
  bingos
) where

import Control.Arrow ((&&&), second)
import Control.Monad
import Data.Char (digitToInt)
import Data.Either
import Data.Either.Extra
import Data.Function (on)
import Data.List (sortBy)
import qualified Data.List as List
import Data.Map (Map)
import Data.Set (Set)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.Maybe (fromMaybe, listToMaybe)
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Read as Text
import Data.Void (Void)
import Day (Day (Day))
import Text.Megaparsec hiding (Pos)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer hiding (space)

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . fmap (uncurry (*) . second (sum . Set.toList . head) . head . findBingo) . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . fmap (uncurry (*) . second (sum . Set.toList . head) . last . findBingo) . parseInput

data Input = Input
             { draws :: [Int]
             , boards :: [Board]
             }
             deriving stock (Show, Eq)

newtype Board = Board [[Int]]
              deriving stock (Show, Eq)

mkBoard :: [[Int]] -> Board
mkBoard = Board

findBingo :: Input -> [(Int, [Set Int])]
findBingo Input{draws,boards} = filter (not . null . snd) $ findBingo' Set.empty draws boards' []
  where
    findBingo' :: Set Int -> [Int] -> [(Board, [Set Int])] -> [(Int, [Set Int])] -> [(Int, [Set Int])]
    findBingo' _ [] bs wins = reverse wins
    findBingo' drawn (d:ds) bs wins =
      let drawn' = d `Set.insert` drawn
          matchBoards = findMatches bs drawn'
      in  findBingo' drawn' ds (filter ((`notElem` matchBoards) . fst) bs) ((d, unmatched drawn' <$> matchBoards):wins)

    unmatched :: Set Int -> Board -> Set Int
    unmatched drawn (Board rows) = Set.fromList (join rows) `Set.difference` drawn

    boards' :: [(Board, [Set Int])]
    boards' = (id &&& bingos) <$> boards

    findMatches :: [(Board, [Set Int])] -> Set Int -> [Board]
    findMatches bs drawn = fst <$> filter (any (`Set.isSubsetOf` drawn) . snd) bs

bingos :: Board -> [Set Int]
bingos (Board rows) =
  let columns = [ [ rows !! i !! j
                  | i <- [0..4] ]
                | j <- [0..4] ]

  in (Set.fromList <$> rows) <> (Set.fromList <$> columns)


type P = Parsec Void Text

parseInput :: Text -> Either Text Input
parseInput = mapLeft (Text.pack . show) . runParser inputP "<none>"

inputP :: P Input
inputP = Input <$> (decimal `sepBy` char ',')
               <*> (some newline *> boardP `sepEndBy` some newline)

boardP :: P Board
boardP = mkBoard <$> ((hspace *> (decimal `sepBy1` hspace)) `sepEndBy1` newline)
