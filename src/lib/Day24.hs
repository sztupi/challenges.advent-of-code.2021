module Day24 (
  day,
) where

import Control.Arrow (Arrow ((***)), first, second, (&&&), (|||))
import Control.Monad (guard, join, void, (<=<))
import Control.Monad.Extra (MonadPlus)
import Data.Bifunctor (Bifunctor (bimap))
import qualified Data.Bits as Bits
import qualified Data.Char as Char
import Data.Either.Extra (mapLeft)
import Data.Functor (($>))
import qualified Data.List as List
import qualified Data.List.Extra as List
import Data.Map (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe (fromMaybe, mapMaybe, maybeToList)
import qualified Data.Maybe as Maybe
import Data.MemoTrie (memo)
import qualified Data.PQueue.Min as PQ
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Read as Text
import Data.Tuple (swap)
import Data.Void (Void)
import Day (Day (Day))
import qualified Debug.Trace as Debug
import Text.Megaparsec hiding (Pos, match)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer hiding (space)
import Data.Tuple.Extra (snd3)
import Data.Vector as V
import Control.DeepSeq (deepseq)
import Control.Monad (when)

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . fmap solve1 . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . const ""

solve1 :: Input -> V.Vector Int
solve1 input = List.head $ List.take 100 $ do
  w <- V.fromList <$> wg
  deepseq (Debug.traceShow w ()) $ void $ pure ()
  deepseq (Debug.traceShow "\n" ()) $ void $ pure ()
  let zzz = zz w
  -- let (Right iz) = interpret (fromIntegral <$> V.toList w) input startSt
  -- when (fromIntegral (z iz) /= zzz ! 14) $
  --   error (show (z iz) <> " /= " <> show (zzz ! 14))
  deepseq (Debug.traceShow zzz ()) $ void $ pure ()
  guard $ zzz ! 14 == 0
  [ w ]

d = V.fromList [ 0,  1,  1,  1, 26,  1,  1,  1, 26, 26,  1, 26, 26, 26,  26 ]
a = V.fromList [ 0, 11, 14, 10,  0, 12, 12, 12, -8, -9, 11, 0,  -5, -6, -12 ]
b = V.fromList [ 0,  8, 13,  2,  7, 11,  4, 13, 13, 10,  1, 2,  14,  6,  14 ]


zz :: V.Vector Int -> V.Vector Int
zz w = V.fromList (List.take 15 z')
  where
    z' :: [Int]
    z' =
          [ 0 ] <>
          [ zn | n <- [1..]
              , let zn =
                      let zp = z' !! (n-1)
                      in if w ! (n-1) == zp `mod` 26 + (a ! n)
                            then zp `div` (d ! n)
                            else (zp `div` (d ! n)) * 26 + (w ! (n-1)) + (b ! n)
          ]


-- w1-4=w14
-- w2+7=w13
-- w3+2=w4
-- w5+6=w12
-- w6-5=w9
-- w7+5=w8
-- w10+1=w11

wg :: [[Int]]
wg = do
  -- n <- [39999999999999,39999999999998 .. 10000000000000]
  -- n <- [51131616112781,51131616112781 .. 51131616112781]
  n <- [92793949489995,92793949489995 .. 92793949489995]
  let w = [ d | q <- [13,12 .. 0]
              , let d = (n `div` 10^q) `mod` 10
              ]
  _ <- if List.all (== 0) (List.drop 8 w) then Debug.traceShow w $ pure () else pure ()
  guard $ 0 `List.notElem` w
  [ w ]

type Input = [Instruction]

data Reg
  = X | Y | Z | W
  deriving (Eq, Ord, Show)

data Val
  = VReg Reg
  | VVal Integer
  deriving (Eq, Ord, Show)

data Instruction
  = Inp Reg
  | Add Reg Val
  | Mul Reg Val
  | Div Reg Val
  | Mod Reg Val
  | Eql Reg Val
  deriving (Eq, Ord, Show)

data IState
  = IState
  { x :: Integer, y :: Integer, z :: Integer, w :: Integer }
  deriving (Eq, Ord, Show)

startSt :: IState
startSt = IState 0 0 0 0

interpret :: [Integer] -> [Instruction] -> IState -> Either Text IState
interpret input instr st =
  case interpret1 input instr st of
    Left err -> Left err
    Right (_, [], st') -> Right st'
    Right (input', instr', st') -> interpret input' instr' st'

interpret1 :: [Integer] -> [Instruction] -> IState -> Either Text ([Integer], [Instruction], IState)
interpret1 [] ((Inp _):_) _ = Left "no input"
interpret1 (i:inps) ((Inp r):insts) st = Right (inps, insts, upd r i st)
interpret1 inps ((Add r v):insts) st = Right (inps, insts, upd r (get r st + eval v st) st)
interpret1 inps ((Mul r v):insts) st = Right (inps, insts, upd r (get r st * eval v st) st)
interpret1 inps ((Div r v):insts) st = Right (inps, insts,
                                              let p = get r st
                                                  q = eval v st
                                              in upd r (abs p `div` abs q * signum p * signum q) st)
interpret1 inps ((Mod r v):insts) st = Right (inps, insts, upd r (get r st `mod` eval v st) st)
interpret1 inps ((Eql r v):insts) st = Right (inps, insts, upd r (if get r st == eval v st then 1 else 0) st)

upd :: Reg -> Integer -> IState -> IState
upd X v st = st { x = v }
upd Y v st = st { y = v }
upd Z v st = st { z = v }
upd W v st = st { w = v }

get :: Reg -> IState -> Integer
get X IState { .. } = x
get Y IState { .. } = y
get Z IState { .. } = z
get W IState { .. } = w

eval :: Val -> IState -> Integer
eval (VReg r) st = get r st
eval (VVal i) _ = i


type P = Parsec Void Text

parseInput :: Text -> Either Text Input
parseInput = mapLeft (Text.pack . show) . runParser inputP ""

inputP :: P Input
inputP = instP `sepEndBy` newline

instP :: P Instruction
instP = choice
        [ string "inp " >> Inp <$> regP
        , string "add " >> Add <$> regP <*> valP
        , string "mul " >> Mul <$> regP <*> valP
        , string "div " >> Div <$> regP <*> valP
        , string "mod " >> Mod <$> regP <*> valP
        , string "eql " >> Eql <$> regP <*> valP
        ]

regP :: P Reg
regP = choice [ char 'x' $> X
              , char 'y' $> Y
              , char 'z' $> Z
              , char 'w' $> W
              ] <* hspace

valP :: P Val
valP = choice [ VReg <$> regP
              , VVal <$> signed space decimal <* hspace
              ]
